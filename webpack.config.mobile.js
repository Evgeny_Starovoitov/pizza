var path = require('path')
var webpack = require('webpack')
const { VueSSRClientPlugin } = require('vue-ssr-webpack-plugin')

module.exports = {
	entry: {
		client: './vue-mobile/index.js'
	},
	output: {
		path: path.join(__dirname, "public/js/mobile/builds/"),
		publicPath: '/js/mobile/builds/',
		filename: '[name].js'
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					loaders: {
						js: 'babel-loader?presets[]=es2015'
					}
				}
			},
			{
			  test: /\.js$/,
			  exclude: /(node_modules|bower_components)/,
			  use: {
			    loader: 'babel-loader?presets[]=es2015'
			  }
			},
			{
				test: /\.js$/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['es2015']
					}
				},
				exclude: /node_modules/
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				loader: 'file-loader',
				options: {
					name: '[name].[ext]?[hash]'
				}
			},

		]
	},
	resolve: {
		extensions: ['.js'],
		alias: {
			'vue$': 'vue/dist/vue.esm.js'
		}
	},
	devServer: {
		historyApiFallback: true,
		noInfo: true,
		headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
    }
	},
	performance: {
		hints: false
	},
	plugins: [
		// this splits the webpack runtime into a leading chunk
		// so that async chunks can be injected right after it.
		// this also enables better caching for your app/vendor code.
		new webpack.optimize.CommonsChunkPlugin({
			name: 'manifest',
			minChunks: Infinity
		}),
		// this will generate the client manifest JSON file.
		new VueSSRClientPlugin()
	],
	devtool: '#eval-source-map',
	
}

if (process.env.NODE_ENV === 'production') {
	module.exports.devtool = '#source-map'
	// http://vue-loader.vuejs.org/en/workflow/production.html
	
	module.exports.plugins = (module.exports.plugins || []).concat([
		new webpack.DefinePlugin({
		  'process.env': {
		    NODE_ENV: '"production"'
		  }
		}),
		new webpack.optimize.UglifyJsPlugin({
		  sourceMap: false,
		  compress: {
		    warnings: false
		  }
		}),
		new webpack.LoaderOptionsPlugin({
		  minimize: true
		})
	])
}