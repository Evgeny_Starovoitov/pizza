'use strict';
// Req: Node
let os = require('os');
let fs = require('fs');
let path = require('path');
// Req: Additional
let _ = require('lodash');
var JSON5 = require('json5');
//
function requireFirst(arrNames) {
	let result = {};
	let fn = null;
	let results = [];
	let fns = [];
	for (let i=0;i<arrNames.length;i++) {
		try {
			fn = path.join(__dirname, arrNames[i]);
			// -- console.log('? ', i, 'fn = ', fn);
			fs.accessSync(fn, fs.F_OK);
			// Load
			let strResult = fs.readFileSync(fn).toString();// require(fn);
			// Parse using `json5`
			let resultN = JSON5.parse(strResult);
			if (resultN) {
				// -- console.log('- result = ', result);
				results.push(resultN);
				fns.push(arrNames[i]);
			}
		} catch (e) {
			// Not accessible
			// -- console.log('?', i, e);
		}
	}
	// Extend
	if (results.length>0) {
		for (let i=0;i<results.length;i++) {
			// -- log.debug('[cfg] 0 result.elastic = ', result.elastic);
			// -- log.debug('[cfg] 1 result[i].elastic = ', results[i].elastic);
			result = _.extend(result, results[i]);
			// -- log.debug('[cfg] 2 result.elastic = ', result.elastic);
		}
	}
	console.info('[cfg]: allConfigs = ' + JSON.stringify(fns));
	// Add dirs
	if (!result.dirs) {
		result.dirs = {};
	}
	if (!result.dirs.base) {
		result.dirs.base = path.join(__dirname, '/../../');
	}
	console.info('[cfg]: pathProject = ' + result.dirs.base);
	return result;
}

// Init
let hostName = os.hostname();
console.info('[cfg]: hostName = [' + hostName + ']');
let cfgNames = [
	// Global `'cfg.json'` | .json5
	'./cfg.json',
	'./cfg.json5',
	'./cfg.roles.json',
	'./cfg.roles.json5',
	// Local (in git) `'~.{hostName}.cfg.json'` | .json5
	'./' + hostName + '.cfg.json',
	'./' + hostName + '.cfg.json5',
	// Local (git-ignored) `'~.{hostName}..cfg.json'` | .json5
	'./~.' + hostName + '.cfg.json',
	'./~.' + hostName + '.cfg.json5'
];
let cfg = requireFirst(cfgNames);
console.info('[cfg]: ProjectName = ' + cfg.projectName);
console.info('[cfg]: PORT = ' + cfg.port);
module.exports = cfg;
