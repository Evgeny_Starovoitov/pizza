'use strict';
// Requires: Mongoose
let mongoose = require('mongoose');
/*
!!: Native promise
var Promise = require("bluebird");
Promise.config({
	// Enables all warnings except forgotten return statements.
	warnings: {
		wForgottenReturn: false
	}
});
//
var Mongoose = Promise.promisifyAll(require("mongoose"));
*/
// Requires: Cfg
let config = require('./cfg');
// let log = require('./log');
// Requires: Models
var Person = require('./model/Person');
var Order = require('./model/Order');
// var Orders = require('./model/Orders');
var Action = require('./model/Action');
// var Geo = require('./model/Geo');
// var Hotel = require('./model/Hotel');
// var PromoCertificate = require('./model/PromoCertificate');

var state = {
	isOpen: false,
	isError: false
}

function init() {
	// log.verbose('[mongoose]: init');
	// Use native promises
	if (global.Promise) {
		mongoose.Promise = global.Promise;
	}	
	//
	mongoose.connect((config.mongoose.uri), (config.mongoose.options));
	mongoose.connection.on('error', function () {
		state.isError = true;
		console.log('[mongoose]: Connection error: ' + JSON.stringify(arguments))
		// log.error('[mongoose]: Connection error: ' + JSON.stringify(arguments));
	});
	mongoose.connection.once('open', function () {
		state.isOpen = true;
		console.log("[mongoose]: open")
		// log.verbose("[mongoose]: open");
	});
}

module.exports = {
	// Mongoose
	mongoose: mongoose,
	// State
	state: state,
	// Models
	Person: Person,
	Action: Action,
	Order: Order,
	// Geo: Geo,
	// Hotel: Hotel,
	// Orders: Orders,
	// PromoCertificate:PromoCertificate,
	// Initialization
	init: init
}