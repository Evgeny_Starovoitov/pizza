var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./cfg');
// var socket = require('./io');
var passport = require('passport');
var session = require('express-session');
var compression = require('compression');
var RedisStore = require('connect-redis')(session);
// req.device.type.toLowerCase();
var device = require('express-device');
// Redis
var redis = require('./redis');
// Setup Agenda
require('./agenda').init(app);
var app = express();

app.use(function (req, res, next) {
	res.setHeader('X-Powered-By', 'dimon');
	res.setHeader('X-Content-Type-Options', 'nosniff');
	res.setHeader('X-XSS-Protection','1; mode=block');
	res.setHeader('X-Frame-Options', 'DENY');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
var proxy = require('http-proxy-middleware');
app.get('/js/builds/:ost', function(req, res, next) {
	if (app.get('env') === 'development') {
		let ost = req.params.ost;
		res.redirect('http://localhost:8080/js/builds/'+ost);
	} else {
		next();
	}
});
app.get('/js/mobile/builds/:ost', function(req, res, next) {
	if (app.get('env') === 'development') {
		let ost = req.params.ost;
		res.redirect('http://localhost:8080/js/mobile/builds/'+ost);
	} else {
		next();
	}
});
require('./dust-helpers').init(app);
// require('./js/cache').init();
app.use(compression({filter: shouldCompress}));

function shouldCompress(req, res) {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header
    return false;
  }

  // fallback to standard filter function
  return compression.filter(req, res);
}
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
// catch app mobile
app.use(function(req, res, next) {
	if(req.headers['x-requested-with'] == 'com.susi.pizza'){
		if(req.originalUrl){
			 return res.render('newapp');
		}
	}
	if((req.headers['x-requested-with'] == 'com.pizza.susi') || (req.headers['x-requested-with'] == 'com.pi-art.susi')){
		console.log('This mobile app');
	}
	next();
});
// Session
var sessionOptions = {
	secret: config.session.secret,
	resave: config.session.resave,
	saveUninitialized: config.session.saveUninitialized,
	cookie: { expires: new Date('2030-07-07T00:00:00Z') } // ГОД
};
if (config.session.redisOptions && redis.client) {
	var redisOpts = JSON.parse(JSON.stringify((config.session.redisOptions)));
	redisOpts.client = redis.client;
	sessionOptions.store = new RedisStore(redisOpts);
}
app.use(session(sessionOptions));
app.use(device.capture());
// Static
app.use(express.static(path.join(__dirname,'../', 'public')));
require('./db').init(app);
// Passport
require('./routes/auth/passport')(passport);
app.use(passport.initialize());
app.use(passport.session());

// auth
require('./routes').init(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  err.url = req.headers.host+req.url;
  next(err);
});

app.use(function(err, req, res, next) {
	if (err){
		// console.log(err);
		var url = (err.url) ? err.url : null;
		var status = err.status || 500;
		var message = (err.status && (err.status == 404)) ? 'Извините но такой страницы нет :(' : 'Извините произошла ошибка ;(';
		var errs = (app.get('env') === 'development') ? JSON.stringify(err, null, ' ') : null;
		res.status(status);
		var error = {error: status,	message:message, url:url, err:errs};
		res.render('error', error);
	}
});

module.exports = app;
