// Standard mongoose requires
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
// Tools
var tools = require('./_tools');
// Inlines
var _Thing = require('./_Thing');
// Schema definition, see documetation in .md file
// Действие
// Функциональный аналог http://schema.org/Action
var actionSchema = new Schema( tools.merge( _Thing.Part, {
// Цель реакции
	leftMenu: { type: Boolean, index: false },
	visible: { type: Boolean, index: false },
	target: { type: String, index: true }, // Название коллекции, содержащей объект `targetId`
	targetId: { type: ObjectId, index: true }, // Идентификатор объекта
	targetOwner: { type: ObjectId, index: true }, // Владелец объекта `targetId` (тип `Person`)
// Значения
	valueReact: { type: Number, index: true }, // Значение реакции (`1`=положительное, `-1`=отрицательное), испольуется вместе с `kindReact`
	valueRating: Number, // Значение рейтинга
	valueCredits: { type: Number, index: true } // Кредиты
}), { strict: false });


// Create and export model
module.exports = mongoose.model('Action', actionSchema);