// Standard mongoose requires
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
var bcrypt = require('bcrypt-nodejs');
// Tools
var tools = require('./_tools');
// Inlines
var _Thing = require('./_Thing');
// // Актуальные заявки
// var _recentApplications = require('./inline/_recentApplications');
// Персона
var personSchema = new Schema( tools.merge( _Thing.Part, {
// Свойства
	// Аутентификация
	auth: {
		local: {
			login: { type: String, index: true },
			password: { type: String, index: false },
			passwordOriginal: { type: String, index: false }
		}
		// vkontakte: {
		// 	id: { type: String, index: true },
		// 	token: { type: String, index: true },
		// 	name: { type: String, index: false },
		// 	avatar: { type: String, index: false }
		// }
	},
	address: {type: [String], index: false},
	bonus: {type: Number, index: false},
	awaitsBonus: {type: Number, index: false},
	cancellation: [{bonus: {type: Number, index: false}, date: {type: Date, index: false}}],
	// address: { type: String, index: true },
	contactPoint: {
		email: { type: String, index: true },
		phone: { type: String, index: true }
	}
}), { strict: false });

// Старый вариант
personSchema.methods.generateHash = function(password) { // no-unused-vars
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
personSchema.methods.validPassword = function(password) {
	return bcrypt.compareSync(password, this.auth.local.password);
};

// Create and export model
module.exports = mongoose.model('Person', personSchema);
