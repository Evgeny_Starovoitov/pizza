// Requires
var _Thumbnail = require('./_Thumbnail');
// Inline schema definition, see documetation in .md file
// Описание изображения
// Аналог http://schema.org/ImageObject
var _ImageObject = {
// Cвойства
	// Опционально может содержать `_id`, который должен соответсвовать Action.Upload
	// В этом случае возможно комментирование и реакции
	// Заголовок
	name: String,
	// Тип объекта ("image"/"video"/...)
	kind: String,
	// Урл оригинала картинки
	url: String,
	// Ширина (в px)
	width: Number,
	// Высота (в px)
	height: Number,
	// Размер оригинала в байтах
	size: Number,
	// SHA1 хеш оригинала (в hex)
	sha1: String,
	// Миниатюры
	thumbnail: [_Thumbnail]
};

module.exports = _ImageObject;
