// Standard mongoose requires
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// Inline schema definition, see documetation in .md file
// Либо ссылка на элемент либо текстовое значение.
var _LinkOrValue = {
// Cвойства
	// Ссылка на элемент
	link: { type: String, index: false },
	// Текстовое значение
	value: { type: String, index: false }
};

module.exports = _LinkOrValue;
