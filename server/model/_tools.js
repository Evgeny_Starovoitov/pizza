module.exports = {
	merge:
		/**
		* Merges obj1's values with obj2's values and returns merged object
		* @param obj1
		* @param obj2
		* @returns obj3 a new object based on obj1 and obj2
		*/
		function merge(obj1, obj2) {
			var obj3 = {};
			for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
			for (var attrname2 in obj2) { obj3[attrname2] = obj2[attrname2]; }
			return obj3;
		}
};
