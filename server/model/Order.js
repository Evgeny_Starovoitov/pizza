// Standard mongoose requires
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
// Tools
var tools = require('./_tools');
// Inlines
var _Thing = require('./_Thing');
// Schema definition, see documetation in .md file
// Действие
// Функциональный аналог http://schema.org/Action
var orderSchema = new Schema( tools.merge( _Thing.Part, {
// Цель реакции
	// название организации
	organization: { type: String, index: false },
	customer: {
		name: { type: String, index: false },
		// телефон по регекспу должен проходить ^(8|\+?\d{1,3})?[ -]?\(?(\d{3})\)?[ -]?(\d{3})[ -]?(\d{2})[ -]?(\d{2})$
		phone: { type: String, index: false },
		email: { type: String, index: false },
	},
	userId: { type: ObjectId, index: true },
	bonus: {
		potratil: { type: Number, index: true },
		nakopil: { type: Number, index: true }
	},
	order: {
		// дата DateTimeUTC или null (если null то ставится сейчас автоматом)
		date: { type: Date, index: false },
		items: [{
			id: { type: String, index: false },
			code: { type: String, index: false },
			name: { type: String, index: false },
			amount: { type: Number, index: false },
			modifiers: Schema.Types.Mixed
		}],
		address: {
			city: { type: String, index: false },
			street: { type: String, index: false },
			home: { type: String, index: false },
		},
		// телефон по регекспу должен проходить ^(8|\+?\d{1,3})?[ -]?\(?(\d{3})\)?[ -]?(\d{3})[ -]?(\d{2})[ -]?(\d{2})$
		phone: { type: String, index: false },
		// модификатор самовывоза
		orderTypeId: { type: String, index: false },
		comment: { type: String, index: false },
		// сумма заказа
		fullSum: { type: Number, index: false },
	},
	paymentType: { type: String, index: false },
	iikoOrderId: { type: String, index: false },
	success: { type: Number, index: false },
	sberOrderId: { type: String, index: false },
	orderStatus: { type: Number, index: false },
	errorMessage: { type: String, index: false }
}), { strict: false });

// Create and export model
module.exports = mongoose.model('Order', orderSchema);