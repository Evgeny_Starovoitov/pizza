// Standard mongoose requires
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
// Inlines
// var _ImageObject = require('./inline/_ImageObject');
// var _PostalAddress = require('./inline/_PostalAddress');

// Inline schema definition, see documetation in .md file
var _Thing = {
// Дополнительные свойства
	// Публичный идентификатор объекта
	urlId: { type: String, index: true },
	// Владелец объекта, т.е. тот, кто его создал.
	owner: { type: ObjectId, index: true },
	// Предок объекта. Используется для поддержки древовидных структур
	parentId: { type: ObjectId, index: true },
	// Даты по объекту
	dates: {
		// Дата создания объекта
		created : { type: Date, index: true },
		// Дата изменения объекта
		changed : { type: Date, index: true },
		// 
		birthDate: { type: Date, index: true }
	},
	// Доступ к объекту
	access: {
		level: { type: Number, index: true }
	},
	// Древо типа
	typeTree: { type: String, index: true },
	// Подтип
	kind: { type: String, index: true },
	// Свойства, определённые в http://schema.org/Thing
	// Наименование
	name: { type: String, index: true },
	// Описание.
	//	Используется для хранения текста комментария, текста поста и т.д.
	description: { type: String, index: false },
	// Картинки/Фотографии.
	image: {type: [String], index: false},
	// Прямая ссылка на объект, на основе `urlId`.
	// NOTE:
	//	Не должен включать домен, т.е. не "http://www.example.com/user/:urlId", а только "/user/:urlId"
	url: { type: String, index: true },
	// Дополнительные свойства
	props: Schema.Types.Mixed,
	meta: Schema.Types.Mixed,
	cache: Schema.Types.Mixed,
	// Приватные свойства (только для владельца)
};

module.exports = {
	Part: _Thing
};

