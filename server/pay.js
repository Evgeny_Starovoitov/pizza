// https://iiko.biz:9900/api/0/orders/add?access_token=t7MNcla4N5URjJuVbr6vlRv-Y4dVvT7PtMkAFYGKPWpNDXGtkqOtS7IN6Hi5f9t-0rSvv_qSkDmycrrfUPu94Q2

'use strict';
let $ = require('./js/api-v1/$');
var https = require('https'),
axios = require('axios'),
axios = axios.create({
	httpsAgent: new https.Agent({  
		rejectUnauthorized: false
	})
}),

nodemailer = require('nodemailer'),
//	опции node-mailer
transporter = nodemailer.createTransport({
	host: 'smtp.yandex.ru',
	auth: {
		user: 'no_reply@poedimdoma.ru',
		pass: '741852963'
	}
});
	
/* GET home page. */
let pay = {};

// Авторизация
pay.auth = async () => {
	const body = await axios.get('https://iiko.biz:9900/api/0/auth/access_token?user_id=susi&user_secret=Admin123')
	return body.data
}

pay.post = (async (url, objToSend) => {
	let date = new Date();

	const body = await axios.post(url, objToSend)
	if (body && body.data && body.data.message) {
		await sendError({ errors: JSON.stringify(body.data), from: `axios error request: ${body.data.message}` })
		throw new Error(body.data.message)
	} else {
		return body.data
	}
})

pay.get = (async (url) => {
		let date = new Date();
		const data = await axios.get(url)

		if (data && data.data && data.data.message) {
			console.log('AXIOS_ERR: ', data.data.message)
			await sendError({ errors: JSON.stringify(data.data), from: `axios error request SMS ${body.data.message}` })
			throw new Error(data.data.message)
		} else {
			return data.data
		}
})

let sendError = async ({ errors, from, subject = 'Ошибка PoedimDoma.Ru', to = 'ddivitay@gmail.com' }) => {
	var htmlToSend = `
		<br /> ${new Date().toTimeString().slice(0,8)} - ${new Date().toISOString().slice(0,10)} <br />
		<br /> Ошибка : ${errors} <br />
	`

	to += ', support@itdo.ru'

	if (errors.indexOf('abbit') > -1) {
		// для iiko заголовок
		subject = 'Зависла очередь rabbit mq Суши пицца красная'
	} else {
		// более развернуто
		htmlToSend += `<br /> Ошибка из :  ${from}`
	}

	var mailOptions = {
		to,
		subject,
		html: htmlToSend,
		text: 'Ошибка ' + errors,
		from: ' <no_reply@poedimdoma.ru>',
	}
	return transporter.sendMail(mailOptions)
}

pay.sendOrder = (async (data, token) => {
	let __data
	try {
		/** данные */
		var objToSend = JSON.parse(JSON.stringify(data));
		__data = objToSend

		// const items = __data.order.items.map(item => `${item.name} - ${item.amount} шт.;  `).join(' , ')
		// const comment = __data.order.comment
		// const adderess = `Адресс: ${__data.order.address.street}, ${__data.order.address.home}${(__data.order.address.housing) ? '/' + __data.order.address.housing : '' }`
		// const fullSumm = `Общая сумма: ${__data.order.fullSum} ;`
		// const contact = `Телефон: ${__data.order.phone}`
		// const bonuses = `Бонусами: ${bonus}`

		// let messgsOrder = encodeURI(`${items}. ${comment}. ${adderess}. ${fullSumm}. ${contact}`)
		// // 79885224499
		// let reqSms = 'https://api3.sms-agent.ru/v2.0/index.php?act=send&login=sushi-pizzayeisk&pass=YAui7lgs&from=Sushi-Pizza&to=79885224499&text=' + messgsOrder;

		// await pay.get( reqSms, null )

		objToSend.organization = (!objToSend.organization) ? "d6460b98-d9a4-11e3-8bac-50465d4d1d14" : objToSend.organization;
		objToSend.order.date = (objToSend.order && !objToSend.order.date) ? null : objToSend.order.date;
		objToSend.order.orderTypeId = (objToSend.order && !objToSend.order.orderTypeId) ? "c2eade9c-a81b-458e-9430-b3db323e6e7e" : objToSend.order.orderTypeId;
		objToSend.order.discountCardTypeId = "ffda0a4b-93f6-4c94-b99f-9050c6a8745a";
		objToSend.deliveryTerminalId = "a7bb3deb-11a4-4e3c-0144-8b760962b55c";

		/** URL на отправку заказа по api */
		let _url = "https://iiko.biz:9900/api/0/orders/add?access_token=" + token;
		console.log('JSON.stringify(data)', JSON.stringify(objToSend))

		// return
		/** данные end */

		/** Делаем запрос в iiko на создание заказа */
		let resultIIko = await pay.post(_url, objToSend)
		if (resultIIko && resultIIko.number && data && data._id && resultIIko.orderId) {
			data.iikoOrderId = String(resultIIko.number)
			/** Сохраняем в базу */
			await $._saveInBase(data)
		}

		/** нет orderId */
		if (!resultIIko || resultIIko && !resultIIko.orderId) {
			throw new Error('resultIIko.orderId not found')
		}
		
		console.log('resultIIko', resultIIko)
		/** Проверяем номер телефона */
		if (!resultIIko.customer || (resultIIko.customer && isNaN(Number(resultIIko.customer.phone)))) throw new Error('resultIIko.customer.phone sms not wil be send')
		
		let phonex = 
					( resultIIko.customer.phone.indexOf('7') == 0 ) ? '+' + resultIIko.customer.phone :
					( ( resultIIko.customer.phone.indexOf('8') == 0) || ( resultIIko.customer.phone.indexOf('+') == 0 ) ) ? resultIIko.customer.phone : null
		
		if (!phonex)  throw new Error(`Phone number invalid format ${phonex}`)

		/** Отправка смс */
		// let messgs_client = encodeURI('Заказ № ' + resultIIko.number + ' принят в обработку. Телефон доставки +79885224499');
		// var reqSms_client = 'https://api2.sms-agent.ru/v2.0/?act=send&login=sushi-pizzayeisk_1&pass=XgpABGcg&from=Sushi-Pizza&to=' + String(phonex) + '&text=' + messgs_client;
		// const statusMsg = await pay.get( reqSms_client, null )
		// console.log({ msgStatus: statusMsg })
		let messgs_dostavka = encodeURI('Новый заказ. №: ' + resultIIko.number + '. Телефон: ' + phonex);
		var reqSms_dostavka = 'https://api3.sms-agent.ru/v2.0/index.php?act=send&login=sushi-pizzayeisk&pass=YAui7lgs&from=Sushi-Pizza&to=79885224499&text=' + messgs_dostavka;
		await pay.get( reqSms_dostavka, null )

		resultIIko.success = true
		return resultIIko

	} catch (e) {
		let date = new Date();
		const ERROR = (e && e.response && e.response.data && JSON.stringify(e.response.data)) || String(e)

		console.log('ERROR', ERROR)
		console.log('create order error ', date, ' ', e)
		await sendError({ errors: ERROR, from: 'pay.sendOrder'})

		return { success : false }
	}
});

pay.checkOrder = (async (data, token) => {
	try {
		let date = new Date();
		var objToSend = JSON.parse(JSON.stringify(data));
		objToSend.organization = (!objToSend.organization) ? "d6460b98-d9a4-11e3-8bac-50465d4d1d14" : objToSend.organization;
		objToSend.order.date = (objToSend.order && !objToSend.order.date) ? null : objToSend.order.date;
		objToSend.order.orderTypeId = (objToSend.order && !objToSend.order.orderTypeId) ? "c2eade9c-a81b-458e-9430-b3db323e6e7e" : objToSend.order.orderTypeId;
		objToSend.order.discountCardTypeId = "ffda0a4b-93f6-4c94-b99f-9050c6a8745a";
		objToSend.deliveryTerminalId = "a7bb3deb-11a4-4e3c-0144-8b760962b55c";
		console.log('JSON.stringify(data) check', JSON.stringify(objToSend))

		let _url = "https://iiko.biz:9900/api/0/orders/checkCreate?access_token=" + token;

		let body = await axios.post(_url, objToSend)
		if (body && body.data && body.data.resultState && body.data.resultState == '0') {
			return false
		}
		if (body && body.data && body.data.problem) {
			await sendError(JSON.stringify({ err: body.data, toSend: objToSend }), 'axios error checkOrder ' + date);
			throw new Error(body.data.problem)
		}
		if (body && body.data && body.data.httpStatusCode && (body.data.httpStatusCode >= 400)) {
			if (body.data.description) {
				sendError(JSON.stringify({ err: body.data, toSend: objToSend }), 'axios error checkOrder ' + date);
				throw new Error(body.data.description)
			}
			if (body.data.message) {
				await sendError(JSON.stringify({ err: body.data, toSend: objToSend }), 'axios error checkOrder ' + date);
				throw new Error(body.data.message)
			}
		}
		return false
	} catch (e) {
		let date = new Date();
		console.log('checkOrder error ', date, ' ', e)
		await sendError(String(e), 'pay.checkOrder ' + date)
	}
})

pay.getHistoryByPerson = (async (token, phone) => {
	let organizationId = 'd6460b98-d9a4-11e3-8bac-50465d4d1d14';
	const body = await axios.get('https://iiko.biz:9900/api/0/orders/deliveryHistoryByPhone?access_token=' + token + '&organization=' + organizationId + '&phone=' + phone)
	return body.data
})

pay.getStreets = (async (token) => {
	let organizationId = 'd6460b98-d9a4-11e3-8bac-50465d4d1d14';
	const body = await axios.get('https://iiko.biz:9900/api/0/streets/streets?access_token=' + token + '&organization=' + organizationId + '&city=b090de0b-8550-6e17-70b2-bbba152bcbd3')
	return body.data
})

module.exports = pay;

// 89892119028
// var obj = {
//     "actualTime": null,
//     "address": {
//         "apartment": null,
//         "city": "Ейск",
//         "comment": "",
//         "doorphone": null,
//         "entrance": null,
//         "externalCartographyId": null,
//         "floor": null,
//         "home": "21/1",
//         "housing": null,
//         "index": null,
//         "regionId": null,
//         "street": "Энгельса",
//         "streetClassifierId": "23011001000016500",
//         "streetId": "0b9c42ad-e762-5c05-0139-7624f15d064c"
//     },
//     "billTime": null,
//     "cancelTime": null,
//     "closeTime": null,
//     "comment": null,
//     "conception": null,
//     "confirmTime": "2017-09-27 19:12:11",
//     "courierInfo": null,
//     "createdTime": "2017-09-27 19:12:10",
//     "customer": {
//         "additionalPhones": [],
//         "addresses": [],
//         "balance": 0,
//         "birthday": null,
//         "cards": null,
//         "comment": "",
//         "cultureName": null,
//         "email": "djvonavi@gmail.com",
//         "externalId": null,
//         "favouriteDish": null,
//         "id": "10f78e4f-9243-43f8-a364-e50932dccd91",
//         "imageId": null,
//         "isBlocked": false,
//         "middleName": null,
//         "name": "Тест",
//         "nick": null,
//         "phone": "+79615139805",
//         "sex": 0,
//         "shouldReceivePromoActionsInfo": null,
//         "surName": null
//     },
//     "customerId": "10f78e4f-9243-43f8-a364-e50932dccd91",
//     "deliveryCancelCause": null,
//     "deliveryCancelComment": null,
//     "deliveryDate": "2017-09-27 20:12:10",
//     "deliveryTerminal": {
//         "address": null,
//         "crmId": "37461",
//         "deliveryTerminalId": "a7bb3deb-11a4-4e3c-0144-8b760962b55c",
//         "externalRevision": 7517653,
//         "restaurantName": "Красная (Суши-Пицца): Доставка",
//         "technicalInformation": null
//     },
//     "discount": 0,
//     "discounts": null,
//     "durationInMinutes": 60,
//     "guests": [
//         {
//             "id": "c0c146dc-489f-e681-015e-c3ff82a43e3b",
//             "name": "Тест"
//         }
//     ],
//     "iikoCard5Coupon": null,
//     "items": [
//         {
//             "amount": 3,
//             "category": null,
//             "code": "10660",
//             "comboInformation": null,
//             "guestId": "c0c146dc-489f-e681-015e-c3ff82a43e3b",
//             "id": "c73df897-3694-4c27-a742-714c7f164460",
//             "modifiers": [
//                 {
//                     "amount": 1,
//                     "category": null,
//                     "code": null,
//                     "comboInformation": null,
//                     "groupName": null,
//                     "id": "6cb74b54-7064-4922-b8fb-ea1908b3ab16",
//                     "name": "- роллы домой?",
//                     "sum": 0
//                 }
//             ],
//             "name": "(Д)Ролл Агава 180гр.",
//             "sum": 810
//         }
//     ],
//     "marketingSource": null,
//     "number": "76268",
//     "operator": null,
//     "orderId": "4f8c940f-35b9-4fb0-aabc-ead07c2486d1",
//     "orderLocationInfo": {
//         "latitude": 46.711987,
//         "longitude": 38.281544
//     },
//     "orderType": {
//         "externalRevision": 4742670,
//         "id": "c2eade9c-a81b-458e-9430-b3db323e6e7e",
//         "name": "Заказ с сайта",
//         "orderServiceType": "DELIVERY_BY_COURIER"
//     },
//     "organization": "d6460b98-d9a4-11e3-8bac-50465d4d1d14",
//     "payments": [
//         {
//             "additionalData": null,
//             "isExternal": false,
//             "isPreliminary": true,
//             "isProcessedExternally": false,
//             "paymentType": {
//                 "applicableMarketingCampaigns": null,
//                 "code": "CASH",
//                 "combinable": true,
//                 "comment": "",
//                 "deleted": false,
//                 "externalRevision": 4742670,
//                 "id": "09322f46-578a-d210-add7-eec222a08871",
//                 "name": "Наличные"
//             },
//             "sum": 810
//         }
//     ],
//     "personsCount": 1,
//     "printTime": null,
//     "problem": {
//         "hasProblem": true,
//         "problem": "Точка доставки Красная (Суши-Пицца): Доставка не соответствует заданным ограничениям."
//     },
//     "restaurantId": "d6460b98-d9a4-11e3-8bac-50465d4d1d14",
// "sendTime": null,
//     "splitBetweenPersons": false,
//     "status": "Новая",
//     "sum": 810
// }


// {
// 	"order": {
// 		"date": null,
// 			"items": [
// 				{
// 					"id": "4d6fa0df-0bc7-4c1f-addc-976d52d70b95",
// 					"code": "10675",
// 					"name": "Американский",
// 					"amount": 1,
// 					"modifiers": [
// 						{
// 							"id": "6cb74b54-7064-4922-b8fb-ea1908b3ab16",
// 							"amount": 1
// 						}
// 					]
// 				}
// 			],
// 				"address": {
// 			"city": "Ейск",
// 				"street": "Армавирская",
// 					"home": "45",
// 						"housing": "",
// 							"apartment": "",
// 								"entrance": "",
// 									"floor": "",
// 										"doorphone": "",
// 											"comment": ""
// 		},
// 		"phone": "79002507499",
// 		"fullSum": 240,
// 		"comment": "ч:5 ДОСТАВКА К 23:00! /картой курьеру",
// 		"personsCount": 5,
// 		"paymentItems": [
// 						{
// 								"sum": 200,
// 								"paymentType": {
// 									"id": "6b7a2856-3b1b-4bd1-82f7-ec68c98190ca"
// 								},
// 								"isProcessedExternally": false
// 							},
// 							{
// 								"sum": 40,
// 								"paymentType": {
// 									"id": "fa5b68cd-e3dd-4092-b073-64a4eaf6a818"
// 								},
// 								"isProcessedExternally": true
// 							}
// 						],
// 							"orderTypeId": "c2eade9c-a81b-458e-9430-b3db323e6e7e",
// 								"discountCardTypeId": "ffda0a4b-93f6-4c94-b99f-9050c6a8745a"
// 	},
// 	"customer": {
// 		"name": "Тест Тест Тест",
// 			"phone": "79002507499",
// 				"email": ""
// 	},
// 	"organization": "d6460b98-d9a4-11e3-8bac-50465d4d1d14",
// 		"deliveryTerminalId": "a7bb3deb-11a4-4e3c-0144-8b760962b55c"
// }


