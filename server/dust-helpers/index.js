'use strict';
var adaro = require('adaro');
var path = require('path');
let cfg = require('../cfg');
String.prototype.fixedEncodeURI = function() {
	var newStr = '';
	for (var i=0; i < this.length; i++) {
		var char = this.charAt(i);
		if (char === '+'){
			newStr+= '%2B';
		} else if (char === '['){
			newStr+= '%5B';
		} else if (char === ']'){
			newStr+= '%5D';
		} else {
			newStr+= char;
		}
	}
	return newStr;
};

var dustOptions = {
	cache: ((cfg && !(cfg.dustCache === null) && !(cfg.dustCache === undefined) && (cfg.dustCache === false)) ? cfg.dustCache : true),
	helpers: [
		function (dust) { dust.optimizers.format = function(ctx, node) { return node; }; },
		
		function (dust) {
			dust.filters.getDay = function(val) {
				if (!val) return '';
				var days = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
				var dateArr = val.split('.');
				var dateStr = dateArr[1]+'/'+dateArr[0]+'/'+dateArr[2];
				var iso = new Date(dateStr);
				return days[iso.getDay()];;
			};
		},
		function (dust) {
			dust.filters.time = function(time) {
				var dateTime = new Date(time);
				var hours = '0' + dateTime.getHours();
				var minutes = '0' + dateTime.getMinutes();
				var day = dateTime.getDate();
				var month = '0' + (Number(dateTime.getMonth())+1);
				var year = dateTime.getFullYear();
				return hours.substr(-2) + ':' + minutes.substr(-2) + ' ' + day + '/' + month.substr(-2) + '/' +year;
			};
		},
		function (dust) {
			dust.filters.port1 = function(val) {
				if (!val) return '';
				return val.split('-')[0];
			};
		},
		function (dust) {
			dust.filters.port2 = function(val) {
				if (!val) return '';
				return val.split('-')[1];
			};
		},
		function (dust) {
			dust.filters.port3 = function(val) {
				if (!val) return '';
				return val.split('-')[2];
			};
		},
		function (dust) {
			dust.filters.price = function(val) {
				var price = String(val);
				var newPrice = [];
				var counter = 1;
				for (let i=(price.length-1); i > -1; i--) {
					var char = price[i];
					if (((counter % 3) === 0) && !(i === 0)) {
						newPrice.unshift(' '+char);
						counter = 1;
					} else {
						counter++;
						newPrice.unshift(char);
					}
				}
				return newPrice.join('');
			};
		},
		function (dust) {
			dust.filters.allRate = function(val) {
				var booking = (val && val.booking && val.booking.rate) ? val.booking.rate : 0;
				var tripadvisor = (val && val.tripadvisor && val.tripadvisor.rate) ? val.tripadvisor.rate : 0;
				var tophotel = (val && val.tophotel && val.tophotel.rate) ? val.tophotel.rate : 0;
				var count = 0;
				if (booking) count++;
				if (tripadvisor) count++;
				if (tophotel) count++;
				if (booking) {
					booking = (5*booking)/10;
				}
				return ((booking+tripadvisor+tophotel)/count).toFixed(1);
			};
		},
		function (dust) {
			dust.filters.hstrs = function(val) {
				var arr = {
					401: '<i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>',
					402: '<i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>',
					403: '<i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star-outline"></i>',
					404: '<i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star mdi-24px"></i><i class="mdi mdi-star mdi-24px"></i>',
					405: 'Apts'
				}
				var resp = '';
				if (arr[val]) {
					resp = arr[val];
				}
				return resp;
			};
		},
		function (dust) {
			dust.filters.mainImage = function(val) {
				var img = 'background3.jpg';
				val = val.template;
				if (val && val.hotel && val.hotel.props) {
					var props = val.hotel.props;
					if (props.booking && props.booking.image && props.booking.image[0]) {
						img = props.booking.image[0];
					} /*else if (props.tripadvisor && props.tripadvisor.image && props.tripadvisor.image[0]) {
						img = props.tripadvisor.image[0];
					} else if (props.tophotels && props.tophotels.image && props.tophotels.image[0]) {
						img = props.tophotels.image[0];
					}*/
				}
				return img;
			};
		},
		function (dust) {
			dust.filters.littleBig = function(val) {
				// http://hotels.sletat.ru/i/f/58176_0.jpg
				// http://hotels.sletat.ru/i/p/58176_0_128_128_1.jpg
				return val.replace('/f/', '/p/').replace('.jpg', '_128_128_1.jpg');
			};
		},
		function (dust) {
			dust.filters.mServices = function(data) {
				if (!data || !data.props || !data.props.services || !data.props.services.main || !data.props.services.main.length) return '';
				var arr = data.props.services.main;
				var str = '';
				for (var i=0;  i < arr.length; i++) {
					var one = arr[i]; 
					if (one && one.Id) {
						switch (one.Id) {
							case '1': 
								str+= '<div class="col-md-6"><i class="mdi mdi-wifi icon-list wifi-help services-pop" ></i><span class="webui-popover-content">Интернет</span></div>';
							break;
							case '24':
								str+= '<div class="col-md-6"><i class="mdi mdi-snowflake icon-list snow-help services-pop"></i><span class="webui-popover-content">Кондиционер</span></div>'; 
							break;
							case '64': 
								str+= '<div class="col-md-6"><i class="mdi mdi-pool icon-list pool-help services-pop" ></i><span class="webui-popover-content">Бассейн</span></div>';
							break;
							case '110': 
								str+= '<div class="col-md-6"><i class="mdi mdi-beach icon-list beach-help services-pop" ></i><span class="webui-popover-content">Близко к пляжу</span></div>';
							break;
							default: 
							break;
						}
					} 
				}
				return str;
			};
		},
		function (dust) {
			dust.filters.mServicesMob = function(data) {
				if (!data || !data.props || !data.props.services || !data.props.services.main || !data.props.services.main.length) return '';
				var arr = data.props.services.main;
				var str = '';
				for (var i=0;  i < arr.length; i++) {
					var one = arr[i]; 
					if (one && one.Id) {
						switch (one.Id) {
							case '1': 
								str+= '<div class="col-md-6"><i class="mdi mdi-wifi icon-list wifi-help services-pop" ></i><p>Интернет</p></div>';
							break;
							case '24':
								str+= '<div class="col-md-6"><i class="mdi mdi-snowflake icon-list snow-help services-pop"></i><p>Кондиционер</p></div>'; 
							break;
							case '64': 
								str+= '<div class="col-md-6"><i class="mdi mdi-pool icon-list pool-help services-pop" ></i><p>Бассейн</p></div>';
							break;
							case '110': 
								str+= '<div class="col-md-6"><i class="mdi mdi-beach icon-list beach-help services-pop" ></i><p>Близко к пляжу</p></div>';
							break;
							default: 
							break;
						}
					} 
				}
				return str;
			};
		},
		function (dust) {
			dust.filters.idToIcon = function(val) {
				var ico = '';
				switch (val) {
					case '1':
						ico = 'wifi';
					break;
					case '6':
						ico = 'beach';
					break;
					case '9':
						ico = 'smoking-off';
					break;
					case '13':
						ico = 'emoticon-happy';
					break;
					case '2':
						ico = 'clipboard-account';
					break;
					case '7':
						ico = 'briefcase';
					break;
					case '10':
						ico = 'silverware-variant';
					break;
					case '14':
						ico = 'pool';
					break;
					case '18':
						ico = 'baby-buggy';
					break;
					case '3':
						ico = 'parking';
					break;
					case '5':
						ico = 'run';
					break;
					case '8':
						ico = 'snowflake';
					break;
					case '11':
						ico = 'car';
					break;
					case '12':
						ico = 'basket-fill';
					break;
					case '15':
						ico = 'seat-flat';
					break;
					case '19':
						ico = 'glasses';
					break;
					default:
						ico = 'beach';
					break;
				}
				return ico;
			};
		},
		function (dust) {
			dust.filters.dateRange = function(val) {
				var months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
				var days = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
				var humanDate = function (d) {
					if (!d) return '';
					var dateArr = d.split('.');
					var dateStr = dateArr[1]+'/'+dateArr[0]+'/'+dateArr[2];
					var iso = new Date(dateStr);
					return iso.getDate()+' '+months[iso.getMonth()];
				}
				var start = humanDate(val[12]);
				var end = humanDate(val[13]);
				return start+' - '+end;
			}
		},
		function (dust) {
			dust.filters.bichLine = function(val) {
				var dflt = 'более 300м.';
				if (!val) {
					return dflt;
				}
				if (val && val.GetHotelInformationResult && val.GetHotelInformationResult.HotelFacilities && val.GetHotelInformationResult.HotelFacilities.HotelInfoFacilityGroup) {
					var arr = val.GetHotelInformationResult.HotelFacilities.HotelInfoFacilityGroup;
					var i=0;
					while(i < arr.length) {
						var one = arr[i];
						if (one && one.Id && (one.Id == '6') && one.Facilities && one.Facilities.HotelInfoFacility && one.Facilities.HotelInfoFacility.Id) {
							var id = one.Facilities.HotelInfoFacility.Id;
							if (id == '110') {
								dflt = 'менее 100м.';
							} else if (id == '111') {
								dflt = 'менее 200м.';
							} else {
								dflt = 'более 300м.';
							}
							break;
						}
						i++;
					}
					return dflt;
				} else {
					return dflt;
				}
			}
		},
		'dustjs-helpers'	
	]
};
module.exports = {
	dustOptions:dustOptions,
	init : function(app){
		app.engine('dust', adaro.dust(dustOptions));
		// app.engine('js', adaro.js(dustOptions)); 
		app.set('views', path.join(__dirname, '../../', 'public/views'));
		// app.set('view engine', 'js');
		app.set('view engine', 'dust');
	} 
};