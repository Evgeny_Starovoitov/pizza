'use strict';
var async = require('asyncawait/async');
var await = require('asyncawait/await');
let render = require('../render'),
	checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$"),
	express = require('express'),
	redis = require('../../redis'),
	router = express.Router(),
	cfg = require('../../cfg'),
	$ = require('../../js/api-v1/$');

let template = {};
function cutUser(user){
	if(user){
		let cutUser = JSON.parse(JSON.stringify(user));
		delete cutUser.auth;
		return cutUser;
	} else {
		return null;
	}
}
router.use((req,res,next)=>{
	template = {
		'ssrVue': 'tours',
		'url': {
			query: {}
		},
		'main': {
			'country': {},
			'departCity': {},
			'city': {},
			'hotel': {}
		},
		'layout':'main',
		'view': '',
		'list': {},
		'meta': ((cfg && cfg.meta && cfg.meta.default) ? cfg.meta.default : {})
	};
	let platform = (req.device.type.toLowerCase() === 'phone') ? 'isMobile' : (req.device.type.toLowerCase() === 'tablet') ? 'isTablet' : (req.device.type.toLowerCase() === 'desktop') ? 'isDesktop' : null;
	// let platform = 'isDesktop';
	if (platform) {
		template[platform] = true;
	}
	template.user = cutUser(req.user);
	if(req.query){
		template.url.query = req.query;
	}
	next();
});

router.route('/menu/:select')
	.get((req, res, next) => {
		// template.view = 'main_page';
		// template.layout = 'mainVue';
		// async
		let toStart = async (() => {
			let obj = {};
			obj.default = {};
			try {
				// 
			} catch(err){
				console.log('error as/aw', err);
			}
			return obj;
		});
		toStart()
			.then((data) => {
				template.list = data;
				if(req.query && req.query.json == ''){
					res.send({'template': template});
				} else {
					// res.send({'template': template});
					render(res, 'index', {'template': template});
				}
			})
			.catch((err) => {
				if(err){
					console.log(err);
					// res.send({error:err});
					render(res, 'index', {'template': template});
				}
			});

	});

module.exports = router;