'use strict';
let NodeCache = require('node-cache'),
	exec = require('child_process').exec,
	db = require('../../db'),
	cfg = require('../../cfg'),
	jsonfile = require('jsonfile'),
	path = require('path'),
	fs = require('fs'),
	// Person = require('../../model/Person'),
	// Hotel = require('../../model/Hotel'),
	// Orders = require('../../model/Orders'),
	// PromoCertificate = require('../../model/PromoCertificate'),
	request = require('request');

let roles = (id) => {
	if(cfg && cfg.roles && cfg.roles.isAdmin && cfg.roles.isAdmin[id]){
		return true;
	} else {
		return false;
	}
}

let superRoles = (id) => {
	if(cfg && cfg.roles && cfg.roles.isSuperAdmin && cfg.roles.isSuperAdmin[id]){
		return true;
	} else {
		return false;
	}
}

let cutUser = function(user){
	if(user){
		let cutUser = JSON.parse(JSON.stringify(user));
		delete cutUser.auth;
		return cutUser;
	} else {
		return null;
	}
}
let _getRequestSt = function(url){
	return new Promise(function(resolve,reject){
		request(url, function (err, response , body) {
			if (err){
				reject (err);
			}
			try {
				body = JSON.parse(body);
			} catch (err) {
				reject (err);
				return;
			}
			resolve (body);
		})
	})
}

let _getRequestStr = function(url){
	return new Promise(function(resolve,reject){
		request(url, function (err, response , body) {
			if (err){
				reject (err);
			}
			resolve (body);
		})
	})
}

let _find_ById = function(schema, query){
	return new Promise(function(resolve,reject){
		if(db && db[schema]){
			db[schema].findById(query, (err, data) => {
				if (err){
					return reject (err);
				}
				return resolve (data);
			})
		} else {
			return resolve (null);
		}
	})
}

let _delete = function(schema, query){
	return new Promise(function(resolve,reject){
		if(db && db[schema]){
			db[schema].remove(query, (err, data) => {
				if (err){
					return reject (err);
				}
				return resolve (true);
			})
		} else {
			return resolve (null);
		}
	})
}

let _find_One = function(schema, query){
	return new Promise(function(resolve,reject){
		if(db && db[schema]){
			db[schema].findOne(query, (err, data) => {
				if (err){
					return reject (err);
				}
				return resolve (data);
			})
		} else {
			return resolve (null);
		}
	})
}

let _find_All = function(schema, query){
	return new Promise(function(resolve,reject){
		if(db && db[schema]){
			db[schema].find(query, (err, data) => {
				if (err){
					return reject (err);
				}
				return resolve (data);
			})
		} else {
			return resolve (null);
		}
	})
}

let _saveInBase = function(obj){
	return new Promise(function(resolve,reject){
		if(obj && obj !== undefined){
			obj.save((err)=>{
				if (err){
					return reject (err);
				}
				return resolve(obj);
			})
		} else {
			return resolve(false);
		}
	})
}

let _run_exex = function(q){
	return new Promise(function(resolve,reject){
		exec(q, function (error, stdout, stderr) {
			if (error) {
				resolve(null)
			} else {
				resolve(stdout);
			}
		}); 
	})
}

let updateModifers = function (arr) {
	return new Promise(function(resolve){
		if (!arr || arr.length == 0) return [];
		let sheet = require('../../sheet');

		let out = [];
		let tmp = [];
		let items = {};
		let toRemove = [];
		let i = 0;
		for (i = 0; i < arr.length; i++) {
			arr[i].count = 1;
		}
		for (i = 0; i < arr.length; i++) {
			if (arr[i].id && arr[i].discountPercent && arr[i].discountPercent > 0) {
				arr[i].oldPrice = arr[i].price;
				arr[i].price = arr[i].price - Math.round(arr[i].price * arr[i].discountPercent)
			}
			let o = arr[i];
			items[o.id] = JSON.parse(JSON.stringify(o));
		}
		for (i = 0; i < arr.length; i++) {
			let one = arr[i];
			if (sheet[one.id] && sheet[one.id].group) {
				one.group = [];
				one.activeItem = 0;
				let group = sheet[one.id].group;
				for (let k = 0; k < group.length; k++) {
					let oneGroup = group[k];
					oneGroup.item = items[oneGroup.id]
					oneGroup.item.count = 1
					one.group.push(oneGroup);
					if (oneGroup.id != one.id) {
						toRemove.push(oneGroup.id);
					}
				}
			}
			if (sheet[one.id] && sheet[one.id].mods) {
				one.mods = sheet[one.id].mods;
			}
			tmp.push(one);
		}
		for (i = 0; i < tmp.length; i++) {
			let one = tmp[i];
			if (!toRemove.includes(one.id)) {
				out.push(one);
			}
		}
		return resolve(out);
	})
}

let _saveJsonFile = function(objToSave){
	return new Promise(function(resolve,reject){
		let file = path.join(__dirname, '../../', 'sheet.json')
		fs.writeFile(file, JSON.stringify(objToSave), (err) => {
			if(err){
				console.log('The file has been saved!', err);
				return reject(err);
			}
			return resolve (true);
		  });
	})
}

let sortRender = (out) => {
	let i=0;
     while(i < out.length) {
      out[i].name = out[i].name.replace(/(\(Д\))|(\()|(\))|(ГР\.)|(гр\.)|(пицца)|([0-9])|\//ig, '');
      let one = out[i];
      if (one.mods && one.mods.new) {
       out.splice(i, 1);
       out.unshift(one);
      }
      i++;
     }
     i=0;
     while(i < out.length) {
      let one = out[i];
      if (one.oldPrice) {
       out.splice(i, 1);
       out.unshift(one);
       break;
      }
      i++;
	 }
	return out
}

module.exports = {
	// Person: Person,
	cfg,
	sortRender,
	roles,
	superRoles,
	_saveJsonFile,
	updateModifers,
	_getRequestSt,
	_getRequestStr,
	_delete,
	_find_ById,
	_find_One,
	_find_All,
	_saveInBase,
	_run_exex,
	cutUser
};
