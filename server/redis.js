'use strict';
// Req: redis
let redis = require('redis');
// Req: config
var config = require('./cfg');
// Redis
let redisObj = {
};

redisObj.client = redis.createClient(config.redis||null);

redisObj.client.info('server', function (err, result) { // eslint-disable-line no-unused-vars
	redisObj.infoServer = result;
});

redisObj.autoinc = function (keyName) {
	return new Promise(function (resolve, reject) {
		redisObj.client.incr([keyName], function (err, result) {
			// -- console.log('[redis] incr:', result);
			if (result && !err) {
				resolve(result);
			} else {
				reject(err);
			}
		});
	});
};


redisObj.set = function (key, val) {
	return new Promise(function (resolve, reject) {
		redisObj.client.set(key, JSON.stringify(val), function(error, result) {
			if (error) {
				reject(error);
			} else {
				resolve(result);
			}
		});
	});
};

// время жизни
redisObj.exp = function (key, val) {
	redisObj.client.expireat(key, parseInt((+new Date)/1000) + val);
};

redisObj.get = function (key) {
	return new Promise(function (resolve, reject) {
		redisObj.client.get(key, function(error, result) {
			if (error) {
				reject(error);
			} else {
				if(result){
					resolve(JSON.parse(result));
				}
			}
		});
	});
};

redisObj.del = function (key) {
	return new Promise(function (resolve, reject) {
		redisObj.client.del(key, function(error, result) {
			// -- console.log('get-error: '+JSON.stringify(error));
			// -- console.log('get-result: '+JSON.stringify(result));
			if (error) {
				reject(error);
			} else {
				resolve(JSON.parse(result));
			}
		});
	});
};

redisObj.wrap = function (key, getter) {
	let isNew = false;
	return redisObj.get(key)
		.then(function (result) {
			if (!result) {
				isNew = true;
				return Promise.resolve(getter());
			} else {
				return result;
			}
		})
		.then(function (result) {
			if ( isNew && (result!==null) && (result!==undefined) ) {
				return redisObj.set(key, result)
					.then(function () {
						return result;
					});
			} else {
				return result;
			}
		});
};

module.exports = redisObj;