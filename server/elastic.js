'use strict';
// Requires: elasticsearch
let elasticsearch = require('elasticsearch');
// Requires: Cfg
let config = require('./cfg');
// Requires: lodash
let _ = require('lodash');
// Requires: $
let $ = require('./js/api-v1/$');
//
let log = require('./log');

let esTypeToIndex = [
	'Action.Item',
	'Action.Post',
	'Action.Group',
	'Action.Service',
	'Action.Shop'
];

// Client
let esObj = {
	index: 'kokos',
	typeToIndex: esTypeToIndex,
	client: null,
	add: addToIndex,
	getLifetimeFilter: getLifetimeFilter,
	getSecurityFilter: getSecurityFilter
};

try {
// -- log.debug("[elastic]: cfg = ", config.elastic);
	esObj.client = new elasticsearch.Client(config.elastic); // { host: 'localhost:9200' }
	log.verbose("[elastic]: init");
	esObj.client.ping({
		requestTimeout: 3000,
		// undocumented params are appended to the query string
		hello: "elasticsearch"
	}, function (error) {
		if (error) {
			log.error('[elastic]: ' + JSON.stringify(error));
			esObj.client = null;
		} else {
			log.verbose('[elastic]: open');
		}
	});
} catch (error) {
	log.error('[elastic]: ' + JSON.stringify(error));
	esObj.client = null;
}

function addToIndex(doc) {
	if (!doc) {
		return Promise.resolve(null);
	}
	if (!(esObj.client||null)) {
		return Promise.resolve(doc);
	} else {
		let docId = String(doc._id);
		// prepare esDoc
		let esDoc = $.clone(doc);
		let esType = String(esDoc.typeTree).toLowerCase().replace('.', '_');
		// _id => id
		esDoc.id = docId;
		esDoc = _.omit(esDoc, '_id');
		// Save to ES
		return esObj.client.index({
			index: esObj.index,
			type: esType,
			id: esDoc.id,
			body: esDoc
		});
	}
}

function getLifetimeFilter(dtNow) { // eslint-disable-line no-unused-vars
	if (!dtNow) {
		dtNow = new Date();
	}
	let luceneNow = dtNow.toISOString();
	luceneNow = luceneNow.split(':').join('\\:');
	//
	let result =
		'(' +
			// Filter-out not jet created
			// '-dates.created:["" TO *] OR dates.created:<="' + dtNow + '"' +
			// -- '(!dates.created) OR dates.created:[* TO ' + luceneNow + ']' +
			'dates.created:<' + luceneNow +
		') AND ' +
		'(' +
			// Filter-out dead (null || >now
			// -- '(!dates.removed) OR dates.removed:{' + luceneNow + ' TO *]' +
			'(*:* -dates.removed:[* TO *]) OR (dates.removed:>' + luceneNow + ')' +
		')';
	return result;
}

function getSecurityFilter(user) { // eslint-disable-line no-unused-vars
	// Public only
	let result = 'access.level:5';
	// Enforce Security by `user`, `owner` and `access.level` as filter
	if ( (user) && (user._id) ) {
		let userId = String(user._id||'');
		result =
			'(access.level:5) OR '+
			'(access.level:4) OR ' +
			'(access.level:1 AND (targetOwner:"' + userId + '" OR props.sides:"' + userId + '") ) OR ' +
			'(owner:"' + userId+ '")';
	}
	return result;
}

module.exports = esObj;
