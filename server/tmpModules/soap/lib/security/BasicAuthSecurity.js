"use strict";

var _ = require('lodash');

function BasicAuthSecurity(username, password, defaults) {
  this.Login = username;
  this.Password = password;
  this.defaults = {};
  _.merge(this.defaults, defaults);
}

BasicAuthSecurity.prototype.addHeaders = function(headers) {
  headers.AuthInfo = {
    'Login': this.Login,
    'Password': this.Password
  }
  // headers.AuthInfo = '' + new Buffer((this._username + ':' + this._password) || '').toString('base64');
};

BasicAuthSecurity.prototype.toXML = function() {
  return '';
};

BasicAuthSecurity.prototype.addOptions = function(options) {
  _.merge(options, this.defaults);
};

module.exports = BasicAuthSecurity;
