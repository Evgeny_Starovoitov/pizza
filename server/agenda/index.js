'use strict';
// Requires
let Agenda = require('agenda');
// Requires: Cfg
let config = require('../cfg');
// Requires: os
let os = require('os');

// Exports object
let agendaExports = {
	init: init,
	engine: null,
	done: done
};

function gracefulExit() {
	if (agendaExports.engine) {
		console.log('agenda: graceful exit'); // eslint-disable-line no-console
		agendaExports.engine.stop(function() {
			process.exit(0);
		});
	}
}

function init() {
	var agenda = new Agenda({db: {address: config.mongoose.uri, collection: 'agenda'}});
	agenda.name(os.hostname() + '-' + process.pid);
	agenda.defaultLockLifetime(30000); // 30 sec
	agendaExports.engine = agenda;
	define(agenda);
	agenda.on('ready', function() {
		// Graceful exit
		process.on('SIGTERM', gracefulExit);
		process.on('SIGINT' , gracefulExit);
		// Schedule
		schedule(agenda);
		// Start
		console.log('agenda: start'); // eslint-disable-line no-console
		agenda.start();
	});
	return agendaExports;
}

function done() {
}

// Define handlers
function define(agenda) { // eslint-disable-line no-unused-vars
	let getList = require('./getList');
	getList(agenda);
	let gePaymentStatus = require('./gePaymentStatus');
	gePaymentStatus(agenda);
	let geStopList = require('./geStopList');
	geStopList(agenda);
	let updateBonus = require('./updateBonus');
	updateBonus(agenda);
}

// Schedule handlers
function schedule(agenda) {
	// agenda.schedule('1 seconds', 'getList');
	if(config && config.agenda && (config.agenda.start == true)){
		agenda.every('5 minutes', 'getList');
		agenda.every('1 minute', 'gePaymentStatus');
		agenda.every('1 minutes', 'geStopList');
		agenda.every('24 hours', 'updateBonus');
	}
}

module.exports = agendaExports;