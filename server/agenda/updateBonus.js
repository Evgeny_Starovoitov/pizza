'use strict';
var async = require('asyncawait/async');
var await = require('asyncawait/await');
let $ = require('../js/api-v1/$');
function defineAgenda(agenda) {
	agenda.define('updateBonus', function(job, done) {
		console.log('Проверка stop list')
		var reqs = async (function () {
			let count = 0;
			try {
				const users = await ($._find_All('Person', { awaitsBonus: { $gt: 0 }}))
				count = users.length
				for (let i = 0; i < count; i++ ) {
					let user = users[i]
					user.bonus = (user && user.bonus >= 0) ? Number(user.bonus) + Number(user.awaitsBonus) : Number(user.awaitsBonus)
					user.awaitsBonus = 0
					await ($._saveInBase(user))
				}
			} catch(e){
				console.log('error in async agenda awaitsBonus', e)
			}
			return count;
		});
		reqs()
			.then((data)=>{
				console.log(data);
				console.log('Проверка awaitsBonus выполнена, кол-во: '+ data)
				done();
			})
		
	});	
}

module.exports = defineAgenda;