'use strict';
var async = require('asyncawait/async');
var await = require('asyncawait/await');
let exQery = '';
let $ = require('../js/api-v1/$');
let redis = require('../redis');
function defineAgenda(agenda) {
	agenda.define('getList', function(job, done) {
		console.log('Старт getList')
		var reqs = async function () {
			let data = {};
			try {
				let token = await $._getRequestSt('https://iiko.biz:9900/api/0/auth/access_token?user_id=susi&user_secret=Admin123')
				if(token){
					let mods = false
					let two = await $._getRequestSt('https://iiko.biz:9900/api/0/nomenclature/d6460b98-d9a4-11e3-8bac-50465d4d1d14?access_token='+token)
					let discounts = await $._getRequestSt('https://iiko.biz:9900/api/0/deliverySettings/deliveryDiscounts?organization=d6460b98-d9a4-11e3-8bac-50465d4d1d14&access_token='+token)
					discounts = discounts && discounts.discounts && discounts.discounts.find(discount => discount.id === '24a34260-7f0a-4c21-b03c-e13995e54ded')
					discounts = discounts && discounts.productCategoryDiscounts			
					if(two && two.groups && two.groups.length && two.products && two.products.length && two.revision){
						let version = await(redis.get('menu:version'));
						if((version == two.revision) && mods == false){
							data.actualBase = true;
						} else {
							await redis.set('menu:version',two.revision)
							// Сохраняем группы продуктов
							let groups = two.groups;
							let groupsArr = [];
							for(let i=0;i<groups.length;i++){
								if(groups[i].isIncludedInMenu && groups[i].id !== '275851bf-2e40-473e-bb3d-f36b70aebbd0'){
									// // Русские буквы есть, запускаем переводчик
									// let uriName = groups[i].name.replace(/\(Д\)/ig, '').replace(/ГР\.$/ig, '').replace(/[0-9]/ig,'');
									// uriName = encodeURIComponent(uriName);
									// exQery = "curl --data '' 'https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20170609T140432Z.cc25afe372ded37b.a6419982f429df0c87fdf93b783cdb5fc0749e6b&text="+uriName+"&lang=ru-en'"
									// let coco = await $._run_exex(exQery)
									// coco = JSON.parse(coco);
									// if(coco && coco.text){
									// }
									groups[i].url = String(i) // coco.text[0].toLowerCase().replace(/[^A-Za-z0-9_]/gi,'_').replace(/__/ig, '_').replace(/\_$/ig, '');
									groupsArr.push(groups[i]);
								}
							}
							if(groupsArr && groupsArr[0]){
								// сортировка категорий
								groupsArr.sort((a,b)=>{
									let one = (a.tags && a.tags[0] && !isNaN(a.tags[0])) ? Number(a.tags[0]) : 0;
									let two = (b.tags && b.tags[0] && !isNaN(b.tags[0])) ? Number(b.tags[0]) : 0;
									if (one > two) {
									 return 1
									}
									if (one < two) {
									 return -1
									}
									return 0
								})
								// сохранение в редис
								let setGroups = await redis.set('menu:groups',groupsArr)
								data.setGroups = setGroups;
							}
							// Сохраняем продукты
							let products = two.products;
							let productsArr = [];

							// discount
							if (discounts && discounts.length) {
								discounts.forEach(discount => {
									products = products.map(product => {
										if (product.productCategoryId === discount.categoryId) {
											product.discountPercent	= Number(discount.percent) / 100
										}
										return product
									})
								})
							}

							for(let j=0; j<products.length; j++) {
								if(products[j].id === "444d6b2b-f6a5-48ab-9c3c-003262cf73fb") {
									data.bonus = await redis.set('menu:delivery-price', products[j])
								} else if(products[j].isIncludedInMenu) {
									// Русские буквы есть, запускаем переводчик
									// let uriName = products[j].name.replace(/\(Д\)/ig, '').replace(/гр\.$/ig, '').replace(/ГР\.$/ig, '').replace(/л\.$/ig, '').replace(/мл\.$/ig, '').replace(/[0-9]/ig,''); 
									// uriName = encodeURIComponent(uriName);
									// exQery = "curl --data '' 'https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20170609T140432Z.cc25afe372ded37b.a6419982f429df0c87fdf93b783cdb5fc0749e6b&text="+uriName+"&lang=ru-en'"
									// let coco = await $._run_exex(exQery)
									// coco = JSON.parse(coco);
									// if(coco && coco.text){
									// }
									products[j].url = String(j) // coco.text[0].toLowerCase().replace(/[^A-Za-z0-9_]/gi,'_').replace(/__/ig, '_').replace(/\_$/ig, '').replace(/__/ig, '_');
									productsArr.push(products[j]);
								}
							}
							if(productsArr && productsArr[0]){
								// to do ilya
								await redis.set('menu:products_not_modified', productsArr)
								let toSave = await $.updateModifers(productsArr)
								let setProducts = await redis.set('menu:products', toSave)
								data.setProducts = setProducts;
							}
						}
					}
				}
				// data.groups = await(redis.get('menu:groups'));
				// data.version = await(redis.get('menu:version'));
				// data.products = await(redis.get('menu:products'));
			} catch(e){
				console.log('error in async agenda',e)
			}
			return data;
		}

		reqs()
			.then((data)=>{
				console.log(data);
				console.log('Финиш getList')
				done();
			})
		
	});	
}

module.exports = defineAgenda;

// {
// 	"additionalInfo": null,
// 	"code": "2123",
// 	"description": "",
// 	"id": "444d6b2b-f6a5-48ab-9c3c-003262cf73fb",
// 	"isDeleted": false,
// 	"name": "Доставка до 500 руб",
// 	"seoDescription": null,
// 	"seoKeywords": null,
// 	"seoText": null,
// 	"seoTitle": null,
// 	"tags": null,
// 	"carbohydrateAmount": 0,
// 	"carbohydrateFullAmount": 0,
// 	"differentPricesOn": [],
// 	"doNotPrintInCheque": false,
// 	"energyAmount": 0,
// 	"energyFullAmount": 0,
// 	"fatAmount": 0,
// 	"fatFullAmount": 0,
// 	"fiberAmount": 0,
// 	"fiberFullAmount": 0,
// 	"groupId": null,
// 	"groupModifiers": [
// 			{
// 					"maxAmount": 10,
// 					"minAmount": 0,
// 					"modifierId": "40bcf55b-47e9-4a51-b2b3-106547713e9e",
// 					"required": false,
// 					"childModifiers": [
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "7afd3d48-38e7-467a-98b5-838bd7db83dd",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "9526f4c4-2d07-4f71-9f3f-a2886e37795f",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "511b2f26-0dd0-4560-a3da-8b47eb224f20",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "c00b0599-bdd2-4241-971b-d094684ab53d",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "ebe4775d-4f9c-4479-9f55-7dcd2c5a6602",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "90068871-1c10-40e9-aabd-0eacdabb8681",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "0e19bb93-b360-471f-942c-815ae3c0fb97",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "a6c53fb1-eaf6-4832-b35f-6a260a7967ed",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "7efa4772-c697-4780-90cb-70f290c6885a",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "5bf60bc1-0d4d-49e4-b0bc-be5c311979c7",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "624b19fd-0e2a-49aa-b0cd-7836f05877a1",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "c7c82d2f-7def-4d9f-9c15-2e99474c8ba6",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "c52c9e89-d7c6-4fba-8593-0f9e61f2d13e",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "0ee279ca-0f16-4f74-8c27-7774d9a0f83f",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "57c20526-d106-4396-b191-7892c7f0b8e9",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "83755fdc-74f0-4aa3-b98c-59aa041b1b9d",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "47ee42fd-7d82-4957-b064-406149b50265",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "9ca8535d-3273-4d33-bd11-af50be6cbf36",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "838e5b20-0697-4b5e-9bf2-e3ef27334c38",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "18895042-71cc-4bb7-b2a9-d20db84843de",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							}
// 					],
// 					"childModifiersHaveMinMaxRestrictions": false
// 			},
// 			{
// 					"maxAmount": 10,
// 					"minAmount": 0,
// 					"modifierId": "e8271825-5ebe-4fbe-ba11-df8f11a129d9",
// 					"required": false,
// 					"childModifiers": [
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "64e11379-5d7a-4be3-86c1-d7ea365f6d1d",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "6cb74b54-7064-4922-b8fb-ea1908b3ab16",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							}
// 					],
// 					"childModifiersHaveMinMaxRestrictions": false
// 			},
// 			{
// 					"maxAmount": 10,
// 					"minAmount": 0,
// 					"modifierId": "de293e20-90f6-4f14-9d35-50ddd162b7ef",
// 					"required": false,
// 					"childModifiers": [
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "012077e7-145e-4f3f-bd2b-3a74a3c36659",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "8820aecf-1bc4-4fb8-b1e8-323bb89a68d1",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "50ba9d5e-ccd2-4a7d-86bd-d95170b169e9",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							}
// 					],
// 					"childModifiersHaveMinMaxRestrictions": false
// 			},
// 			{
// 					"maxAmount": 1,
// 					"minAmount": 0,
// 					"modifierId": "83fff89e-9894-41c6-a8ee-ce5a0e3397bd",
// 					"required": false,
// 					"childModifiers": [
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "2bf77eea-9b9e-4381-b778-55b47d48ba43",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							},
// 							{
// 									"maxAmount": 0,
// 									"minAmount": 0,
// 									"modifierId": "031190e1-1adb-45ba-97bc-f19cde7dd9a2",
// 									"required": false,
// 									"defaultAmount": 0,
// 									"hideIfDefaultAmount": false
// 							}
// 					],
// 					"childModifiersHaveMinMaxRestrictions": false
// 			}
// 	],
// 	"measureUnit": "шт",
// 	"modifiers": [],
// 	"price": 50,
// 	"productCategoryId": "05e10eb3-07a0-60cb-014c-58537ae7a0e3",
// 	"prohibitedToSaleOn": [],
// 	"type": "good",
// 	"useBalanceForSell": false,
// 	"weight": 1,
// 	"images": [],
// 	"isIncludedInMenu": true,
// 	"order": 0,
// 	"parentGroup": "275851bf-2e40-473e-bb3d-f36b70aebbd0"
// },