'use strict';
var async = require('asyncawait/async');
var await = require('asyncawait/await');
let $ = require('../js/api-v1/$');
let pay = require('../pay');
function defineAgenda(agenda) {
	agenda.define('gePaymentStatus', function(job, done) {
		console.log('Проверка оплат SBERBANK');
		var reqs = ( async () => {
			let data = {};
			try {
				let allSberOrders = await($._find_All('Order',{"success":0,"paymentType":"sber","sberOrderId":{$exists:true}}))
				let fakePayment = false

				if(allSberOrders && allSberOrders.length){
					for(let i=0; i<allSberOrders.length; i++){
						let mods = false;
						let sbApiLogin = 'poedimdoma-api';
						let	sbApiPassword = 'Admin123';
						let sberOrderId = allSberOrders[i].sberOrderId;
						let checkUrl = 'https://securepayments.sberbank.ru/payment/rest/getOrderStatus.do?orderId='+sberOrderId+'&language=ru&password='+sbApiPassword+'&userName='+sbApiLogin
						let checkPaymentLink = await($._getRequestSt(checkUrl));
						// Поле OrderStatus может принимать следующие значения:
						// 1 Заказ зарегистрирован, но не оплачен
						// 2 Предавторизованная сумма захолдирована (для двухстадийных платежей) Проведена полная авторизация суммы заказа
						// 3 Авторизация отменена
						// 4 По транзакции была проведена операция возврата
						// 5 Инициирована авторизация через ACS банка-эмитента
						// 6 Авторизация отклонена
						allSberOrders[i].orderStatus = checkPaymentLink.OrderStatus;
						allSberOrders[i].errorMessage = checkPaymentLink.ErrorMessage;
						const paymentStatus = allSberOrders[i].orderStatus
						// console.log(paymentStatus, allSberOrders[i])
						if(paymentStatus == 3 || paymentStatus == 4 || paymentStatus == 6){
							allSberOrders[i].success = -1;
							allSberOrders[i].description = 
								paymentStatus == 3 ? 'Авторизация отменена' 
									: paymentStatus == 4 ? 'Инициирована авторизация через ACS банка-эмитента' 
										: paymentStatus == 5 ? 'Время авторизации вышло' 
											: paymentStatus == 6 ? 'Авторизация отклонена' : 'Не известный код операции';
							data[allSberOrders[i]._id] = "false payment"

							mods = true;
							fakePayment = true
						}

						if(allSberOrders[i].orderStatus == 2) {
							data[allSberOrders[i]._id] = "true payment"
							allSberOrders[i].success = 1
							allSberOrders[i].order.comment += ' / sberbank: оплачено!'
							allSberOrders[i].description = 'Оплачено'
							const _token = await(pay.auth())

							const sendOrder = await pay.sendOrder(allSberOrders[i], _token)
							allSberOrders[i].iikoOrderId = sendOrder.number

							mods = true
						}

						if (mods) {
							if (allSberOrders[i].userId) {
								// bonus: {
								// 	potratil: { type: Number, index: true },
								// 	nakopil: { type: Number, index: true }
								// },

								// bonus: {type: Number, index: false},
								// awaitsBonus: {type: Number, index: false},
								let user =  await $._find_One('Person', { _id: allSberOrders[i].userId } )
								
								if (user && user._id) {
									const tratit = allSberOrders[i].bonus && allSberOrders[i].bonus.potratil
									const nakopil = allSberOrders[i].bonus && allSberOrders[i].bonus.nakopil

									// console.log(tratit, nakopil, user, allSberOrders[i])

									if (fakePayment) {
										if (tratit) {
											allSberOrders[i].bonus.potratil = 0
										}
										if (nakopil) {
											allSberOrders[i].bonus.nakopil = 0
										}
									} else {
										if (tratit && tratit > 0) user.bonus = user.bonus ? user.bonus - tratit : - tratit
										if (nakopil && nakopil > 0) user.bonus = user.bonus ? user.bonus + nakopil : nakopil
	
										await $._saveInBase(user)
									}

								}
							}

							allSberOrders[i].dates.changed = Date.now()

							await ($._saveInBase(allSberOrders[i]))
						}
					}
				} else {
					data = "not order, to check payments";
				}
			} catch(e){
				console.log('error in async agenda',e);
			}
			return data;
		});
		reqs()
			.then((data)=>{
				console.log(data);
				console.log('Проверка оплат SBERBANK выполнена')
				done();
			})
		
	});	
}

module.exports = defineAgenda;
