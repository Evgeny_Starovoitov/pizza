'use strict';
var async = require('asyncawait/async');
var await = require('asyncawait/await');
let $ = require('../js/api-v1/$');
let redis = require('../redis');
function defineAgenda(agenda) {
	agenda.define('geStopList', function(job, done) {
		console.log('Проверка stop list')
		var reqs = async (function () {
			let count = 0;
			let data = {};
			try {
				let one = await($._getRequestSt('https://iiko.biz:9900/api/0/auth/access_token?user_id=susi&user_secret=Admin123'));
				let two = await($._getRequestSt('https://iiko.biz:9900/api/0/stopLists/getDeliveryStopList?access_token='+one+'&organization=d6460b98-d9a4-11e3-8bac-50465d4d1d14'));
				if(two && two.stopList && two.stopList.length){
					for(var j=0; j<two.stopList.length; j++){
						if(two.stopList && two.stopList[j].items && two.stopList[j].items.length && (two.stopList[j].deliveryTerminalId == 'a7bb3deb-11a4-4e3c-0144-8b760962b55c')){
							count = two.stopList[j].items.length;
							for(var i=0;i < two.stopList[j].items.length; i++){
								let key = two.stopList[j].items[i].productId;
								let val = (two.stopList[j].items[i].balance && (two.stopList[j].items[i].balance < 0)) ? 0 : two.stopList[j].items[i].balance;
								data[key] = val;
							}
						}
					}
				}
				let setStopList = await(redis.set('menu:stoplist', data));
			} catch(e){
				console.log('error in async agenda stop list',e)
			}
			return count;
		});
		reqs()
			.then((data)=>{
				console.log(data);
				console.log('Проверка Проверка stop list выполнена')
				done();
			})
		
	});	
}

module.exports = defineAgenda;