'use strict';
var ioEngine = require('socket.io');
var socketSearch = require('./socket/socketSearch');
var getRates = require('./socket/socketSendHotelRate');
let async = require('asyncawait/async'),
	await = require('asyncawait/await');
var ioObj = {
	engine: ioEngine,
	io: null,
	init: null
};

function init(http) {
	console.log('[socket.io]: init');
	ioObj.io = ioEngine(http);
	// Connect / Disconnect
	ioObj.io.on('connection', function(socket) {
		ioObj.io.emit('statsChange');
		socket.join(socket.id);
		socket.on('search', function(data){
			getRates('hotel-rate:'+data.cid)
				.then((rates)=>{
					console.log('new-finding');
					data.rates = rates;
					socketSearch(data, socket, ioObj)
						.then((obj)=>{
							if (obj && obj.err){
								throw new Error(obj.err);
							} else {
								console.log('end socket chunk');
								ioObj.io.in(socket.id).emit(data.chunkEnd);
								return;
							}
						})
						.catch((err)=>{
							console.log('Ошибка :', String(err));
							ioObj.io.in(socket.id).emit(data.chunkEnd, String(err));
							return;
						});
				})
		});
		socket.on('search2', function(data){
			getRates('hotel-rate:'+data.cid)
				.then((rates)=>{
					console.log('new-finding-2');
					data.rates = rates;
					socketSearch(data, socket, ioObj)
						.then((obj)=>{
							if (obj && obj.err){
								throw new Error(obj.err);
							} else {
								console.log('end socket chunk 2');
								ioObj.io.in(socket.id).emit(data.chunkEnd);
								return;
							}
						})
						.catch((err)=>{
							console.log('Ошибка-2 :', String(err));
							ioObj.io.in(socket.id).emit(data.chunkEnd, String(err));
							return;
						});
				})
		});
		socket.on('search3', function(data){
			getRates('hotel-rate:'+data.cid)
				.then((rates)=>{
					console.log('new-finding-3');
					data.rates = rates;
					socketSearch(data, socket, ioObj)
						.then((obj)=>{
							if (obj && obj.err){
								throw new Error(obj.err);
							} else {
								console.log('end socket chunk 3');
								ioObj.io.in(socket.id).emit(data.chunkEnd);
								return;
							}
						})
						.catch((err)=>{
							console.log('Ошибка-3 :', String(err));
							ioObj.io.in(socket.id).emit(data.chunkEnd, String(err));
							return;
						});
				})
		});
	});
	ioObj.io.on('disconnect', function(){
		// console.log('[socket.io]: disconnected');
		ioObj.io.emit('statsChange');
	});
	//
	return ioObj;
}

ioObj.init = init;

module.exports = ioObj;

// let scktSrch = async (()=> {
// 	let obj = {};
// 	let url = data.str;
// 	try {
// 		let a = url.replace('requestId=0','requestId='+requestID);
// 		let one = await (socketSearch(data.str, socket, ioObj));
// 		if (one){
// 			let two = await (socketSearch(data.str, socket, ioObj));
// 			if (two){
// 				let three = await (socketSearch(data.str, socket, ioObj));
// 			}
// 		}
// 	} catch (err) {
// 		return object.err = err;
// 	}
// 	return obj;
// });
// scktSrch()
// 	.then((data)=>{
// 		if (data && data.err){
// 			throw new Error(datas.err);
// 		} else {
// 			console.log('end socket chunk');
// 			ioObj.io.in(socket.id).emit('chunkEnd');
// 			return;
// 		}
// 	})
// 	.catch((err)=>{
// 		console.log('Ошибка :', String(err));
// 		ioObj.io.in(socket.id).emit('chunkEnd', String(err));
// 		return;
// 	});