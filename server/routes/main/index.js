'use strict';
var async = require('asyncawait/async');
var await = require('asyncawait/await');
let render = require('../render'),
	tplData,
	_tplData = require('../tplData'),
	checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$"),
	express = require('express'),
	redis = require('../../redis'),
	router = express.Router(),
	pay = require('../../pay'),
	db = require('../../db'),
	$ = require('../../js/api-v1/$'),
	nodemailer = require('nodemailer'),
	//	опции node-mailer
	transporter = nodemailer.createTransport({
		host: 'smtp.yandex.ru',
		auth: {
			user: 'no_reply@poedimdoma.ru',
			pass: '741852963'
		}
	});

router.route('/feedback')
	.post((req, res) => {
		let body = req.body;
		if(body.description && body.phone && body.fio && body.position){
			var htmlToSend = '<br /> ФИО : '+body.fio+' <br /> Должность : '+body.position+'  <br /> Телефон : '+body.phone+' <br /> О себе : '+body.description;
			var mailOptions = {
				from:' <no_reply@poedimdoma.ru>',
				subject: 'Отклик на вакансию',
				text: 'Отклик на вакансию',
				to: 'oksana_muhortova@mail.ru',
				html: htmlToSend
			};
			transporter.sendMail(mailOptions, function(err) {
				if (err) {
					console.log(err)
					return res.send(err);
				}
				// Сохраняем нового пользователя
				return res.send({success:'Ваше обращение будет рассмотрено в ближайшее время.'});
			});
		} else if(body.description && body.phone && body.fio && body.review){
			var htmlToSend = '<br /> ФИО : '+body.fio+'  <br /> Телефон : '+body.phone+' <br /> Отзыв : '+body.description;
			var mailOptions = {
				from:' <no_reply@poedimdoma.ru>',
				subject: 'Отзыв',
				text: 'Отзыв пользователя с сайта',
				to: 'hizovaleksandr@gmail.com, sushi-pizza.office@mail.ru, art-sushi@bk.ru',
				html: htmlToSend
			};
			transporter.sendMail(mailOptions, function(err) {
				if (err) {
					console.log(err)
					return res.send(err);
				}
				// Сохраняем нового пользователя
				return res.send({success:'Ваше обращение будет рассмотрено в ближайшее время.'});
			});
		} else {
			return res.send({error:"Не все поля заполнены"});
		}

	});

router.route('/payment')
	.post((req, res) => {
		const tplData = _tplData(req)

		// проверка состояния бонусной системы
		const ALLOW_BONUS = tplData.allow_bonus

		/** Максимальный процент оплаты бонусами */
		const MAX_BINUS_PERSENT_ON_ORDER = 0.4
		const PROMO_ITEM_ID = 'd11111111111110000000001'
		const ALCOHOL_PARENT_GROUP = '160bbccc-34f4-4ffe-b3e3-f803373f380f'

		let body = req.body;
		let tel = body.order.order.phone.replace(/\D+/g,"")
		if((tel.length === 10) && (String(tel[0]) === '9')){
			tel = '7'+tel;
		}
		if((tel.length === 11) && (String(tel[0]) === '8') && (String(tel[1]) === '9')){
			tel = '7'+(tel.slice(1));
		}

		body.order.order.phone = tel
		body.order.customer.phone = tel
		if(!body || (body && !body.order) || (body && body.order && !body.paymentType)) return res.send({error: "Data not found"});

		let summ = (body.order && body.order.order && body.order.order.fullSum) ? body.order.order.fullSum : null;
		if(!summ || summ === 0 || summ < tplData.minOrderAmount) return res.send({error: `Сумма заказа должна превышать ${tplData.minOrderAmount} рублей`});

		body.order.order.paymentItems = [];

		const meta = body.meta || {}

		let toStart = (async () => {
			/** ЭТО СУММА ВСЕГО, НЕ УЧИТЫВАЯ АКЦИИ И НАПИТКИ */
			var sumBonuses = 0;
			let newOrder = new db.Order();

			const tratitBonus = {
				'status' : false,
				'val' :0
			};

			let obj = {}

			try {
				// obj.err = "На сайте ведутся технические работы. Чтобы сделать заказ, позвоните по телефону: +7 861 32-999-99 или +7 988 522-44-99";
				// return obj;

				if(!body.order.order.items || body.order.order.items.length === 0) {
					obj.err = "ERROR_14, Корзина пуста";
					return obj;
				}

				// check stop list
				const stopList = await redis.get('menu:stoplist')
				const notAllowProduct = body.order.order.items.forEach(cardItem => cardItem.amount >  stopList[cardItem.id])
				if (notAllowProduct) {
					obj.err = "ERROR_15, Некоторые продукты находятся в стоп листе";
					return obj;
				}

				/** тянем промо итем чтобы не начислить бонусы по скидочным товарам */
				let promoItem = await($._find_ById('Action', PROMO_ITEM_ID));
				let promoItemId = (promoItem && promoItem.props && promoItem.props.productId) ? promoItem.props.productId : '';

				const allItems = await(redis.get('menu:products'))

				// check summ

				const fullSum = body.order.order.fullSum
				let totalSumm = 0

				if (allItems && allItems[0]){
					body.order.order.items.forEach(payProduct => {
						if (!payProduct || !payProduct.id) return

						const iikoItem = allItems.find(product => payProduct.id == product.id)


						if (iikoItem) {
							if (iikoItem.id != promoItemId && iikoItem.parentGroup != ALCOHOL_PARENT_GROUP && !iikoItem.oldPrice) {
								sumBonuses += (iikoItem.price * payProduct.amount);
							}
							totalSumm  +=  iikoItem.price * payProduct.amount
							// console.log(totalSumm, iikoItem.price, payProduct.amount, iikoItem)
						} else {
							allItems.forEach(_item=> {
								if (_item && _item.group && _item.group.length) {
									const itm = _item.group.find(product => payProduct.id == product.id)
									
									const item = itm && itm.item
									if (item) {
										if (item.id != promoItemId && item.parentGroup != ALCOHOL_PARENT_GROUP && !item.oldPrice) {
											sumBonuses += (item.price * payProduct.amount);
										}
										totalSumm  +=  item.price * payProduct.amount
										// console.log(totalSumm, item.price, payProduct.amount, item)
									}
								}
							})
						}
					})
				}

				// if (fullSum !== totalSumm) {
				// 	obj.err = "Произошла ошибка, не верная цена";
				// 	return obj
				// }

				// Проверка на доступность бонусов и возможность их потратить
				// "id": "af7fdf3e-a3c2-4bb3-a6ed-3b03ed0e3291", это бонус
				let bonusPay = (ALLOW_BONUS && body.bonus && (+(body.bonus) > 0)) ? Number(body.bonus) : 0;
				if(bonusPay && bonusPay > 0){
					if(req.user && req.user.bonus && (bonusPay <= Number(req.user.bonus))){
						if(bonusPay > (sumBonuses * MAX_BINUS_PERSENT_ON_ORDER)) {
							bonusPay = sumBonuses * MAX_BINUS_PERSENT_ON_ORDER
							onsole.log({ a: req.user, b: req.user.bonus, c: bonusPay, d: Number(req.user.bonus), sumBonuses, summ: sumBonuses * MAX_BINUS_PERSENT_ON_ORDER }, 'Человек пытается потратить бонусов больше чем можно')
						}

						// chunkfullSumm = Number(body.order.order.fullSum) - bonusPay;
						tratitBonus.status = true;
						tratitBonus.val = bonusPay;
						body.order.order.paymentItems.push(
							{
								"sum": bonusPay,
								"paymentType": {
									"id":"af7fdf3e-a3c2-4bb3-a6ed-3b03ed0e3291",
								},
								"isProcessedExternally": false
							}
						)
					} else {
						console.log({ a: req.user, b: req.user.bonus, c: bonusPay, d: Number(req.user.bonus)})
						obj.err = 'Сервер не ответил вовремя, попробуйте повторить попытку снова, произошел сбой'

						res.cookie('connect.sid', '', {expires: new Date(1), path: '/' });
						req.logOut();
						res.clearCookie('connect.sid', { path: '/' });
						res.redirect('/');

						return obj;
					}
				}
				// ------------------- // -------------------- // 
				let _token = await(pay.auth());

				for (var key in body.order){
					if(Object.prototype.hasOwnProperty.call(body.order, key)) {
						newOrder[key] = body.order[key];
					}
				}
				newOrder.meta = meta

				newOrder.paymentType = body.paymentType;
				newOrder.dates.created = Date.now();

				if(body.paymentType == 'sber'){
					newOrder.success = 0
					body.order.order.paymentItems.push({
						"sum": (ALLOW_BONUS && tratitBonus.val && tratitBonus.val > 0) ? ( Number(summ) - tratitBonus.val) : Number(summ),
						"paymentType": {
							"id":"6b7a2856-3b1b-4bd1-82f7-ec68c98190ca",
						},
						"isProcessedExternally": false
					});
				} else if (body.paymentType == 'cash') {
					body.order.order.paymentItems.push({
						"sum": (ALLOW_BONUS && tratitBonus.val && tratitBonus.val > 0) ? ( Number(summ) - tratitBonus.val) : Number(summ),
						"paymentType": {
							"id":"09322f46-578a-d210-add7-eec222a08871",
						},
						"isProcessedExternally": false
					})
				} else if (body.paymentType == 'courier_card') {
					if (body.order && body.order.order && body.order.order.comment){
						body.order.order.comment += ' /картой курьеру';
					}
					if (body.order && body.order.order && body.order.order.fullSum){
						body.order.order.paymentItems.push({
							"sum": (ALLOW_BONUS && tratitBonus.val && tratitBonus.val > 0) ? ( Number(summ) - tratitBonus.val) : Number(summ),
							"paymentType": {
								"id":"6b7a2856-3b1b-4bd1-82f7-ec68c98190ca",
							},
							"isProcessedExternally": false
						})
						// console.log(body.order.order.paymentItems);
					}
				} else {
					obj.err = "Не допустимый способ оплаты";
					return obj;
				}

				newOrder.order.paymentItems = body.order.order.paymentItems;

				if (req.user) {
					newOrder.userId = req.user.id
					if (ALLOW_BONUS) {
						newOrder.bonus = {
							potratil: tratitBonus.val,
							nakopil: tratitBonus.status && tratitBonus.val ? ( ( Number(sumBonuses) - Number(tratitBonus.val) ) * 0.1).toFixed(0) : ( Number(sumBonuses) * 0.1 ).toFixed(0)
						}
					}
				}

				if(body.paymentType != 'sber'){
					obj.payment = await pay.sendOrder(body.order, _token || null)
					if (obj.payment && obj.payment.number) {
						newOrder.iikoOrderId = obj.payment.number || null
					}

					obj.success = "Заказ принят в обработку"
				} else {
					let sbApiLogin = 'poedimdoma-api'
					let	sbApiPassword = 'Admin123'
					let price = (ALLOW_BONUS && tratitBonus.val && tratitBonus.val > 0) ? ( Number(summ) - tratitBonus.val) * 100 : Number(summ) * 100
					let	sbUrl = 'https://securepayments.sberbank.ru/payment/rest/register.do?amount='+String(price)+'&language=ru&orderNumber='+String(newOrder._id)+'&password='+sbApiPassword+'&returnUrl=https://poedimdoma.ru/\?payment=success&userName='+sbApiLogin
					let sberPaymentLink = await($._getRequestSt(sbUrl));
					if(sberPaymentLink && sberPaymentLink.orderId && sberPaymentLink.formUrl){
						// todo
						newOrder.sberOrderId = sberPaymentLink.orderId;

						obj.sberPaymentLink = sberPaymentLink.formUrl;

						await $._saveInBase(newOrder)

						obj.success = "Заказ принят в обработку";
					} else {
						obj.err = "Оплата не прошла";
					}
					// console.log('sberPaymentLink', sberPaymentLink);
				}

			} catch(err){
				console.log('ERROR_2', err)
				obj.err = String(err);
				return obj
			}

			if (obj.payment && obj.payment.success) {
				await $._saveInBase(newOrder)

				if (req.user && ALLOW_BONUS) {
	
					/** Списываем бон */
					if (tratitBonus.status && tratitBonus.val) {
						req.user.bonus = ( Number(req.user.bonus) ) - ( Number(tratitBonus.val) )
					}
	
					if (sumBonuses) {
						sumBonuses = (tratitBonus.status && tratitBonus.val) ? ( ( Number(sumBonuses) - Number(tratitBonus.val) ) * 0.1).toFixed(0) : ( Number(sumBonuses) * 0.1 ).toFixed(0)
						req.user.awaitsBonus = (req.user.awaitsBonus) ? Number(req.user.awaitsBonus) + Number(sumBonuses) : Number(sumBonuses)
					}
					await $._saveInBase(req.user)
				}
			}

			/** Под сомнением бонус на юзера если он не авторизован */
			// if (!req.user && sumBonuses){
			// 	let phone = body.order.order.phone
			// 	let firstLogin = phone;
			// 	let secondLOgin = phone;
			// 	let thirdLogin = phone;
			// 	if(!isNaN(+(phone))){
			// 		// Таким образом мы избавимся от знака  +....
			// 		// И переведем логин в строку чтоб отвалидировать ее
			// 		phone = String(+(phone));
			// 		firstLogin = phone;
			// 		if((phone.length === 10) && (String(phone[0]) === '9')){
			// 			secondLOgin = '7'+phone;
			// 		}
			// 		if((phone.length === 11) && (String(phone[0]) === '8') && (String(phone[1]) === '9')){
			// 			secondLOgin = phone.slice(1);
			// 			thirdLogin = '7'+(phone.slice(1));
			// 		}
			// 	}
			// 	let _usr = await($._find_One('Person',{ $or: [	{'auth.local.login': firstLogin}, {'auth.local.login': secondLOgin}, {'auth.local.login': thirdLogin} ] } ))
			// 	if (sumBonuses) {
			// 		sumBonuses = ( Number(sumBonuses) * 0.1).toFixed(0)
			// 		_usr.awaitsBonus = (_usr.awaitsBonus) ? Number(_usr.awaitsBonus) + Number(sumBonuses) : Number(sumBonuses)
			// 		await($._saveInBase(_usr))
			// 	}
			// }


			return obj;
		});
		toStart()
			.then((data) => {
				if(!data){
					throw new Error("Произошла ошибка. Пожалуйста, совершите заказ по телефону")
				} else if(data.err){
						throw new Error(data.err);
				} else {
					return res.send(data);
				}
			})
			.catch((err) => {
				if(err){
					console.log('reject_err', err)
					return res.send({error:String(err)});
				}
			});
	});

router.use((req, res, next) => {
	tplData = _tplData(req);
	if(req.user && req.user._id){
		tplData.user = $.cutUser(req.user);
		if($.roles(req.user._id)){
			tplData.isAdmin = $.roles(req.user._id)
		}
	}
	let toStart = async () => {
		let obj = {}
		try {
			obj.pages = await $._find_All('Action',{'kind':'pages','visible':true})
			obj.pages.sort((a,b) => {
				let one = (a.props && a.props.sortOrder) ? Number(a.props.sortOrder) : 100;
				let two = (b.props && b.props.sortOrder) ? Number(b.props.sortOrder) : 100;
				if (one > two) {
					return 1;
				}
				if (one < two) {
					return -1;
				}
				return 0;
			});
			tplData.delivery = await redis.get('menu:delivery-price')
			tplData.bonus = $.cfg.bonus
			tplData.version = await redis.get('menu:version')
			tplData.stopList = await redis.get('menu:stoplist')
			tplData.promo.weekly = await $._find_ById('Action','d11111111111110000000001')
			tplData.promo.slider = await $._find_ById('Action','d11111111111110000000002')
			obj.groups = await redis.get('menu:groups')
			const productsResult = await redis.get('menu:products')
			obj.products = $.sortRender(productsResult)
			// 
		} catch (err) {
			console.log('error as/aw', err);
		}
		return obj;
	}

	toStart()
		.then((data) => {
			tplData.list = data;
			next()
			return false;
		})
		.catch((err) => {
			if(err){
				console.log('error tplData',err);
				next()
			}
			return false;
		})

})

router.route('/')
	.get((req, res, next) => {
		// let tplData = _tplData(req);
		tplData.template.view = 'main_page';
		tplData.template.layout = 'mainVue';
		tplData.template.component = 'main-page';
		if(req.query && req.query.json == ''){
			return res.send(tplData);
		} else if((tplData.isMobile == true) || (tplData.isTablet == true)){
			return res.render('index', tplData);
		} else {
			return render(res, 'index', tplData);
		}

	})

router.route('/stop-list')
	.get(async (req, res) => {
		const stopList = await redis.get('menu:stoplist')
		res.send(stopList)
	})

router.route('/category/:url')
	.get((req, res, next) => {
		let url = (req.params.url + '').toLowerCase();
		let errUrl;
		// Для старой версии соответствие урлов
		if(!isNaN(url)){
			switch(url){
				case '15':	url = 'rolls';break;	case '19':url = 'baked_rolls'; break;	case '20':url = 'hot_rolls';break;
				case '18':	url = 'sets';break;	case '7':url = 'pizza';break;	case '24':url = 'on_the_grill';break;
				case '23':	url = 'georgian_cuisine';break;	case '14':url = 'snacks';break;	case '11':	url = 'soups';break;
				case '13':	url = 'burgers_and_sandwiches';break;	case '10':url = 'pasta';break;	case '12':url = 'salads';break;
				case '8':	url = 'focaccia_pies_bread';break;	case '9':url = 'hot_meals';break;	case '6':url = 'garnish_sauce';break;
				case '5':	url = 'desserts';break;	case '21':url = 'drinks';break;
			}
		}
		console.log(url)
		tplData.template.view = 'main_page';
		tplData.template.layout = 'mainVue';
		tplData.template.component = 'category-page';
		// async
		for(var i=0; i< tplData.list.groups.length; i++){
			if(tplData.list.groups[i] && (tplData.list.groups[i].url == url || tplData.list.groups[i].url == req.params.url)){
				tplData.main = tplData.list.groups[i];
			}
		}
		if(!tplData.main.id){
			errUrl = true;
		} else {
			tplData.main.items = [];
			for(var j=0;j<tplData.list.products.length;j++){
				if(tplData.main && tplData.main.id && (tplData.main.id == tplData.list.products[j].parentGroup)){
					tplData.main.items.push(tplData.list.products[j]);
				}
			}
		}
		if(errUrl){
			return next();
		}
		if(req.query && req.query.json == ''){
			return res.send(tplData);
		} else {
			// res.send({'tplData': tplData}); 'isMobile' || 'isTablet'
			if((tplData.isMobile == true) || (tplData.isTablet == true)){
				return res.render('index', tplData);
			} else {
				return render(res, 'index', tplData);
			}
		}
	});

router.route('/item/:id')
	.get((req, res, next) => {
		let id = (req.params.id + '').toLowerCase();
		// Для старой версии соответствие урлов
		tplData.template.view = 'main_page';
		tplData.template.layout = 'mainVue';
		tplData.template.component = 'item-page';
		// async
		if(!checkForHexRegExp.test(id)) return next();
		$._find_ById('Action',id)
			.then((data) => {
				if(data){
					tplData.main = data;
					if(req.query && req.query.json == ''){
						return res.send(tplData);
					} else if((tplData.isMobile == true) || (tplData.isTablet == true)){
						return res.render('index', tplData);
					} else {
						return render(res, 'index', tplData);
					}
				} else {
					next();
				}

			})
			.catch((err) => {
				if(err){
					console.log('error in item/'+id, err)
					next();
				}
			});
	});
// /////////////
// ASYNC
// /////////////
let getOtherPages = function(tplData, url, callback){
	let getOtherPageAndItems = async ((url) => {
		let obj = {};
		try {
			let section = await($._find_One('Action', {'urlId':url,'typeTree':'Action.Section'}));

			if(section && section._id){
				tplData.main = section;
				let query
				if (url === 'actions') {
					query = {'parentId':section._id, 'dates.created': { $gte: new Date("2019-11-25T23:59:59Z").toISOString() } }
				} else {
					query = {'parentId':section._id }
				}
				let arr = await($._find_All('Action',query));
				arr.sort((a,b)=>{
					let one = (new Date(a.dates.created)).getTime();
					let two = (new Date(b.dates.created)).getTime();
					if (one > two) {
						return -1;
					}
					if (one < two) {
						return 1;
					}
					return 0
				});
				tplData.list.items = arr;
			} else {
				return obj.errUrl = true;
			}
		} catch(err){
			console.log('error as/aw', err);
		}
		return obj;
	});
	getOtherPageAndItems(url)
		.then((data) => {
			if(data.errUrl){
				return callback(null, null);
			}
			return callback(null, tplData);

		})
		.catch((err) => {
			if(err){
				return callback(err, tplData);
			}
		});
}
// /////////////
// /////////////
// /////////////
router.route('/:url')
	.get((req, res, next) => {
		let url = (req.params.url + '').toLowerCase();
		// console.log('???lololo');
		if((url != 'actions') && (url != 'news')) return next();
		// console.log('lololo');
		// let tplData = _tplData(req);
		tplData.template.view = 'main_page';
		tplData.template.layout = 'mainVue';
		tplData.template.component = 'simple-page';
		getOtherPages(tplData, url, function(err, result){
			if(err){
				console.log(err);
			}
			if(!result) next();
			if(req.query && req.query.json == ''){
				return res.send(tplData);
			} else if((tplData.isMobile == true) || (tplData.isTablet == true)){
				return res.render('index', tplData);
			} else {
				return render(res, 'index', tplData);
			}
		});
	});

router.route('/pages/:url')
	.get((req, res, next) => {
		let url = (req.params.url + '').toLowerCase();
		// let tplData = _tplData(req);
		tplData.template.view = 'main_page';
		tplData.template.layout = 'mainVue';
		tplData.template.component = 'simple-page';
		getOtherPages(tplData, url, function(err, result){
			if(err){
				console.log(err);
			}
			if(!result) next();
			if(req.query && req.query.json == ''){
				return res.send(tplData);
			} else 	if((tplData.isMobile == true) || (tplData.isTablet == true)){
				return res.render('index', tplData);
			} else {
				return render(res, 'index', tplData);
			}
		});
	});

module.exports = router;

// // Сумма должна быть указана в копейках. Тоесть вместо 100.00 должно быть 10000, для этого умнажаем на 100.
// let price = (+(summ) * 100);
// // После того как пройдет оплата нам нужно на наш обработчик кинуть callback url с orderId который успешно оплачен
// let orderId = orderId; // orderId
// //callback url c orderId, можно накидать сюда все что хотим. Переход будет выполнен на этот url после успешной оплаты
// // поэтому нам нужно передать в query необходимые для нашего обработчика параметры
// let returnUrl = 'http://localhost:5000/\?payment=success&orderId='+orderId; 
// // Финальный url выглядит примерно так
// let	sbUrl = 'https://securepayments.sberbank.ru/payment/rest/register.do?amount=' + String(price) + '&language=ru&orderNumber=' + orderId + '&password=' + password + '&returnUrl=' + returnUrl + '&userName=' + login;