'use strict';
let ssrVue = require('./vue-ssr');
function render(res, templateName, data) {
	res.render(templateName, data, function (err, out){
		if (err) {
			console.error('[renderer] dust: ' + String(err));
		}
		ssrVue(out, data)
			.then((result)=>{
				if (result.err){
					console.error('[renderer.js] Vue Render Error: ' + String(result.err));
				} else {
					return res.send(result);
				}
			})
			.catch((err)=>{
				// if error vue render only dust
				console.error('[renderer.js] Vue Render Error: ' + String(err));
				return res.send(out);
			});
	});
}

module.exports = render;