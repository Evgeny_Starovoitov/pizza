'use strict';
let Rusha = require('rusha'),
	rusha = new Rusha(),
	arrayBufferToBuffer = require('arraybuffer-to-buffer'),
	express = require('express'),
	router = express.Router(),
	path = require('path'),
	fs = require('fs'),
	$ = require('../../js/api-v1/$');

var _appendBuffer = function(buffer1, buffer2) {
	var tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
	tmp.set(new Uint8Array(buffer1), 0);
	tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
	return tmp.buffer;
};

let saveFile = function(buf){
	return new Promise(function(resolve, reject){
		// определение его MIME TYPE
		let type = 'jpеg';
		var arr = (new Uint8Array(buf)).subarray(0, 4);
		var header = "";
		for(var i = 0; i < arr.length; i++) {
		   header += arr[i].toString(16);
		}
		switch (header) {
			case "89504e47":
				type = "png";
				break;
			case "47494638":
				type = "gif";
				break;
			case "ffd8ffe0":
			case "ffd8ffe1":
			case "ffd8ffe2":
				type = "jpeg";
				break;
			default:
				type = "jpeg"; // Or you can use the blob.type as fallback
				break;
		}
		// чтение контрольной суммы
		var hash = rusha.digestFromArrayBuffer(buf);
		let out;
		
		if (buf instanceof ArrayBuffer) {
			// если большой файл
			var v = new DataView(buf);
			[].slice.call('abc').forEach(function(s, i) {
				v[i] = s.charCodeAt(0);
			});
			out = arrayBufferToBuffer(buf);
		} else {
			// если малый
			out = buf;
		}
		var fileName = String(`${path.join(__dirname, '../../../public/img')}/${hash}.${type}`);
		fs.writeFile(fileName, out, function (err) {
			if (err) {
				console.log('err', fileName, err)
				return resolve({error: 'error in save file'});
			} else {
				return resolve({fileName: '/img/' + hash + '.' + type, hash: hash});
			}
		});
	});
};

router.route('/')
	.post((req,res,next) => {
		var buf;
		req.on('data', (data) => {
			if (!buf) {
				buf = data;
			} else {
				buf = _appendBuffer(buf, data);
			}
		});
		req.on('end', () => {
			saveFile(buf)
				.then((resp)=>{
					console.log(resp)
					res.send(resp);
				});
		});
	});
module.exports = router;
