'use strict';
/* global __dirname */
// load all the things we need
var	VKontakteStrategy = require('passport-vkontakte').Strategy;
var hashers = require('node-django-hashers');
var request = require('request');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../cfg'),
	Person = require('../../model/Person'),
	LocalStrategy = require('passport-local').Strategy;

module.exports = function(passport) {
	// =========================================================================
	// Passport session setup ==================================================
	// =========================================================================
	//сериализация и десериализация пользователя, поддерживание постоянных сеансов входа
	passport.serializeUser(function(user, done) {
		done(null, user._id);
	});
	passport.deserializeUser(function(id, done) {
		Person.findById(id, function(err, user) {
			done(err, user);
		});
	});
	// =========================================================================
	// LOCAL LOGIN =============================================================
	// =========================================================================
	passport.use('local-login', new LocalStrategy({
		usernameField: 'login',
		passwordField: 'password'
	}, function(login, password, done) {
				// var user = new Person();
				// user.auth.local.login = login;
				// user.auth.local.password = password;
				// user.save(function(err){
				// 	if (err){
				// 		console.log(err);
				// 	}
				// 	console.log('класс');
				// 	return done(null, user);
				// });
		var firstLogin = login;
		var SecondLOgin = login;
		var thirdLogin = login;
		// Если это цыфра то это номер телефона
		if(!isNaN(+(login))){
			// Таким образом мы избавимся от знака  +....
			// И переведем логин в строку чтоб отвалидировать ее
			login = String(+(login));
			firstLogin = login;
			if((login.length === 10) && (String(login[0]) === '9')){
				SecondLOgin = '7'+login;
			}
			if((login.length === 11) && (String(login[0]) === '8') && (String(login[1]) === '9')){
				SecondLOgin = login.slice(1);
				thirdLogin = '7'+(login.slice(1));
			}
		} else {
			return done(null, false, 'Неверный формат логина');
		}
		Person.findOne({$or:[	{'auth.local.login': firstLogin},
								{'auth.local.login': SecondLOgin},
								{'auth.local.login': thirdLogin}]}, function (err, user) {
			if (err) {
				return done(err); 
			}
			if (!user) {
				return done(null, false, 'Неверный логин или пароль');
			}
			if(!user.auth.local.password){
				user.auth.local.password = user.generateHash(String(password));
				user.auth.local.passwordOriginal = String(password);
			} else {
				if (!bcrypt.compareSync(password, user.auth.local.password)){
					return done(null, false, 'Неверный логин или пароль');
				}
			}
			if(user.props && user.props.confirmation){
				return done(null, user);
			} else {
				if(!user.props) user.props = {};
				user.props.confirmation = 1;
				user.markModified('props');
				user.save((err)=>{
					return done(null, user);
				})
			}
		});
	}));
};