'use strict';
var NodeCache = require('node-cache'),
	authCache = new NodeCache(),
	Person = require('../../model/Person'),
	bcrypt = require('bcrypt-nodejs'),
	request = require('request'),
	nodemailer = require('nodemailer'),
	//	опции node-mailer
	transporter = nodemailer.createTransport({
		host: 'smtp.yandex.ru',
		auth: {
			user: 'no_reply@kokos.travel',
			pass: '741852963'
		}
	});

function signup(req, res, body, done){
	if(body.phone /* && body.email  && body.address */ && body.name && body.password && body.password1 && (body.password == body.password1)){
		Person.findOne({'auth.local.login': String(+(body.phone))}, function(err, user) {
			var time = '';
			if (err) {
				return done(err);
			}
			// проверить , чтобы убедиться, что пользователь не существует с такой электронной почтой
			if (!user) {
				var newUser = new Person();
				newUser.auth = {};
				// newUser.address = body.address;
				newUser.contactPoint = {};
				newUser.contactPoint.phone = body.phone;
				newUser.contactPoint.email = body.email;
				// newUser.contactPoint.email = body.email;
				newUser.auth.local = {};
				newUser.auth.local.login = +(body.phone);
				newUser.auth.local.password = newUser.generateHash(body.password);
				newUser.auth.local.originPassword = body.password;
				newUser.props = {};
				newUser.props.confirmation = 0;
				newUser.dates = {};
				newUser.name = body.name;
				// newUser.dates.birthDate = body.birthDate;
				newUser.dates.created = Date.now();
				newUser.kind = 'user';
				return newUser.save(function(err){
					if(err){
						console.log(err);
						done({error: 'Ошибка попробуйте позже.'});
					} else {
						done(null,{register: true, login: body.phone});
					}
				});
				// Функция отправки кода подтверждения (пароля) для аккаунта
			} else {
				// return  err
				return done({error:'Такой пользователь уже есть. Если вы забыли пароль,попробуйте его восстановить.'});
			}
		});
	} else {
		// return err
		done({error:'Данные заполнены не правильно'});
	}
	return;
}
function rndStr(len) {
	var s ='', abd ='0123456789', aL = abd.length;
	while(s.length < len)
		s += abd[Math.random() * aL|0];
	return s;
}

// Восстановление пароля
function passRecovery(req, res, done){
	var body = req.body;
	var login = body.login;
	if (!login) return done(null, {error:'Ошибка не верно введен логин!'});
	var firstLogin = login;
	var SecondLOgin = login;
	var thirdLogin = login;
	// Если это цыфра то это номер телефона
	if(!isNaN(+(login))){
		// Таким образом мы избавимся от знака  +....
		// И переведем логин в строку чтоб отвалидировать ее
		login = String(+(login));
		firstLogin = login;
		if((login.length === 10) && (String(login[0]) === '9')){
			SecondLOgin = '7'+login;
		}
		if((login.length === 11) && (String(login[0]) === '8') && (String(login[1]) === '9')){
			SecondLOgin = login.slice(1);
			thirdLogin = '7'+(login.slice(1));
		}
	} else {
		return done(null, {error:'Неверный формат логина'});
	}
	Person.findOne({$or:[	{'auth.local.login': firstLogin},
							{'auth.local.login': SecondLOgin},
							{'auth.local.login': thirdLogin}]}, function (err, user) {
		if (err) {
			return done({error: err});
		}
		// Нашли пользователя по логину, который нужно восстановить
		if (user && user._id) {
			login = +(login);
			var time = (Date.parse(new Date())/1000);
			var _cached = authCache.get('user:'+login);
			if (_cached){
				var past = time - _cached.time;
				if ((past) < 60) {
					done(null, {error: 'Повторный запрос кода восстановления возможен через '+ (60-past) +' секунд!'});
					return;
				} 
			} else {
				authCache.set('user:'+login, {time: time});
			}
			let passs = rndStr(6);
			let modify = true;
			if(user.auth && user.auth.local.passwordOriginal){
				passs = user.auth.local.passwordOriginal;
				modify = false;
			}
			let messgs = encodeURI('Ваш пароль : '+passs+' , для доступа на сайт Poedimdoma.ru');
			var reqSms = 'https://api2.sms-agent.ru/v2.0/?act=send&login=sushi-pizzayeisk_1&pass=XgpABGcg&from=Sushi-Pizza&to=' + String(login) + '&text=' + messgs;
			// отправка
			request(reqSms, function (error /*, response , body*/) {
				if(error){
					return done(null, {error: 'Ошибка повторите позже.'});
				}
				if(modify){
					user.auth.local.password = user.generateHash(passs);
					user.auth.local.passwordOriginal = passs;
					user.save((err)=>{
						return done(null, {success:'Пароль успешно отправлен, на ваш номер телефона.'});
					})
				} else {
					return done(null, {success:'Пароль успешно отправлен, на ваш номер телефона.'});
				}
				// });
			});
			// Первый шаг отправка своего логина и получение нового пароля
		} else {
			return done(null, {error: 'Данного пользователя не существует.'});
		}
	});
}

// Редактирование пароля
function passwordEdit(req, body, callback){
	if (!req.user.validPassword(body.passwordOld)) {
		return callback(null, {error : 'Вы ввели не верный пароль'});
	} else {
		req.user.auth.local.password = req.user.generateHash(body.passwordNew);
		req.user.auth.local.originPassword = body.passwordNew;
		req.user.save(function(err){
			if (err) {
				return callback(null, {error: 'Ошибка попробуйте позже'});
			} else {
				return callback(null, {success:'Пароль успешно изменен.'});
			}
		});
	}
}

function sendMessageToSupport(body, done){
	var htmlToSend = body.recepientText.replace(/\n/g, '<br />');
	var mailOptions = {
		from:' <no_reply@kokos.travel>',
		subject: 'Вопрос от ' + body.recepientMail,
		text: 'Техподдержка',
		to: 'support@kokos.travel',
		html: htmlToSend
	};
	transporter.sendMail(mailOptions, function(err) {
		if (err) {
			return done(err);
		}
		// Сохраняем нового пользователя
		return done (null, {success:'Ваше обращение будет рассмотрено в ближайшее время.'});
	});
}

module.exports = {
	sendMessageToSupport:sendMessageToSupport,
	signup:signup,
	passRecovery:passRecovery,
	passwordEdit:passwordEdit
};
