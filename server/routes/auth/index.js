'use strict';
// Устанавливаем зависимости
let reg = require('./_registration'),
	express = require('express'),
	router = express.Router(),
	redis = require('../../redis'),
	passport = require('passport');

	// Функция проверки (авторизован ли User)
// function isLoggedIn(req, res, next) {
// 	if (req.isAuthenticated()) {
// 		return next();
// 	} else {
// 		next({error: 401, message: 'Вы не авторизованы, пройдите авторизацию!', err: {} });
// 	}
// }
	
// ЛОКАЛЬНАЯ АВТОРИЗАЦИЯ
router.route('/login')
	// Принимаем POST из авторизации
	// .get((req, res, next) => {
	// 	console.log('req.sessionID',req.sessionID);
	// })
	.post((req, res) => {
		passport.authenticate('local-login',(err, user, message) => {
			if (err) {
				console.log(err);
				return res.send({error : 'Ошибка'});
			}
			if (!user) {
				if (!message){
					message = 'Ошибка авторизации';
				}
				return res.send({error: message});
			} else {
				req.logIn(user,(err) => {
					if (err) {
						console.log('err');
						return res.send({error: message});
					}
					var newUser = JSON.stringify(user);
					newUser = JSON.parse(newUser);
					if (newUser.auth) delete newUser.auth;
					// Session_middleware
					return res.send({success:true, session: req.sessionID});
				});
			}
		})(req, res);
	});
	// Изменить пароль
router.route('/passwordedit')
	.post((req, res, next) => {
		// isLoggedIn();
		var body = req.body;
		if (req.user){
			if (body.passwordOld && body.passwordNew && body.passwordNew2 && (body.passwordNew == body.passwordNew2)){
				reg.passwordEdit(req, body, (err,result) => {
					if(err){
						return next(err);
					}
					res.send(result);	
				});
			} else {
				res.send({error:'Данные введены не верно.'});
			}
		}
	});

router.route('/registration')
	.post((req,res,next) => {
		var body = req.body;
		reg.signup(req, res, body, (err,result) => {
			if(err){
				console.log('errs', err);
				res.send(err);
			} else {
				if(!result.register){
					// Первый шаг и обработка ошибок
					return res.send(result);
				}
				// Финиш второго шага + авторизация
				if(result.register && result.register === true){
					if(req.body && req.body.password && req.body.phone){
						req.body.password = req.body.password;
						req.body.login = req.body.phone;
					}
					passport.authenticate('local-login', (err, user, message) => {
						if (err) {
							console.log('01',err)
							return next(err); 
						}
						if (!user) {
							if (!message){
								message = 'Ошибка авторизации';
							}
							return res.send({error: message});
						}
						if(result.login){
							req.logIn(user, (err) => {
								if (err) { 
									console.log('req.logIn fail', err)
									return res.send({error:'Ошибка авторизации!'});
								}
								// $.logApi(req, 'auth', 'login', true);
								return res.send({'success': true, session: req.sessionID});
							});
						}
					})(req, res, next);
				}
			}
		});
	});

// Выйти
router.route('/logout')
	.get((req, res) => {
		let _session = (req.query && req.query.session) ? req.query.session : null;
		if(req.user){
				req.logout();  // Выход с учетной записи
				res.redirect('/'); // И естественно переадресация на главную
		} else if (_session){
			redis.del("sess:"+_session)
				.then((data)=>{
					res.send({success: true})
				})
				.catch((err2)=>{
					res.send({error: String(err)})
				})	
		} else {
			res.redirect('/');
		}
	});


// АВТОРИЗАЦИЯ ЧЕРЕЗ СОЦИАЛЬНЫЕ СЕТИ

// vkontakte
router.route('/vkontakte')
	.get(passport.authenticate('vkontakte', { scope: ['email'] }), function(req, res){
		res.redirect('/');
  	});

router.route('/vkontakte/callback')
	.get(passport.authenticate('vkontakte', { failureRedirect: '/' }), function (req, res) {
		// var url = (req && req.user && req.user._id) ? '/user/'+ req.user._id : '/';
		res.redirect((req && req.user && req.user._id) ? '/user/'+ req.user._id : '/');
  	});

// =============================================================================
// ВОССТАНОВЛЕНИЕ или изменение ПАРОЛЯ ЛОКАЛЬНОЙ УЧЕТНОЙ ЗАПИСИ ================
// =============================================================================

router.route('/password/recovery')
	.post(function(req, res, next){
		if (req.isAuthenticated()) {
			res.send({error:'Эта функция доступна только не авторизованным пользывателям.'});
		} else {
			reg.passRecovery(req, res, function(err, result){
				if (err) {
					// Получили ошибку
					console.log('errPassRecovery:', err);
				} else {
					res.send(result);
				}
			});
		}
	})


// =============================================================================
// Отправка письма в тех поддержку =============================================
// =============================================================================

router.route('/support')
	.post(function(req, res, next){
		var body = req.body;
		if (body && body.recepientMail && body.recepientText){
			reg.sendMessageToSupport(body, function(err, result){
				if (err) {
					// Получили ошибку
					return next(err);
				} else {
					res.send(result);
				}
			});
		}
	});

module.exports = router;