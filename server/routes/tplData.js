let $ = require('../js/api-v1/$')

const ALLOW_BONUS = true
const MIN_ORDER_AMOUNT = 300
const MIN_FREE_DELIVERY_AMOUNT = 500

function tplData(req) {
	let platform = (req.device.type.toLowerCase() === 'phone') ? 'isMobile' : (req.device.type.toLowerCase() === 'tablet') ? 'isMobile' : null; //(req.device.type.toLowerCase() === 'desktop') ? 'isDesktop' : null;
	let tplData = {
		'allow_bonus': ALLOW_BONUS,
		'minOrderAmount': MIN_ORDER_AMOUNT,
		'minFreeDeliveryAmount': MIN_FREE_DELIVERY_AMOUNT,
		'url': {
			query: {}
		},
		'main': {},
		'list': {},
		'template':{
			'component':'main-page',
			'view': '',
			'layout':'main'
		},
		'promo': {}
	};
	// let platform = 'isDesktop';
	let host = req.get('host');
	if (platform || host.indexOf("m.poedimdoma.ru") != -1 || (req.query && req.query.mobile == 'true')) {
		tplData.isMobile = true;
	}
	tplData.user = $.cutUser(req.user);
	if(req.query){
		tplData.url.query = req.query;
	}
	return tplData;
}

module.exports = tplData;
