'use strict';
var async = require('asyncawait/async');
var await = require('asyncawait/await');
let render = require('../render'),
	_tplData = require('../tplData'),
	tplData,
	pay = require('../../pay'),
	checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$"),
	express = require('express'),
	request = require('request'),
	redis = require('../../redis'),
	router = express.Router(),
	$ = require('../../js/api-v1/$');

router.use((req, res, next)=>{
	tplData = _tplData(req);
	if(req.user){
		tplData.main = $.cutUser(req.user);
		tplData.user = $.cutUser(req.user);
		if($.roles(req.user._id)){
			tplData.isAdmin = $.roles(req.user._id)
		}
	}
	let toStart = async (() => {
		let obj = {};
		try {
			obj.pages = await($._find_All('Action',{'kind':'pages','visible':true}));
			tplData.stopList = await(redis.get('menu:stoplist'));
			obj.groups = await(redis.get('menu:groups'));
			tplData.version = await(redis.get('menu:version'));
			obj.products = await(redis.get('menu:products'));
			tplData.promo.weekly = await($._find_ById('Action','d11111111111110000000001'))
			tplData.promo.slider = await($._find_ById('Action','d11111111111110000000002'))
			// 
		} catch(err){
			console.log('error as/aw', err);
		}
		return obj;
	});
	toStart()
		.then((data) => {
			tplData.list = data;
			next();
			return;
		})
		.catch((err) => {
			if(err){
				console.log('error tplData',err);
				next();
				return;
			}
		});

});

router.route('/')
	.get((req, res, next) => {
		if(!req.user) return res.redirect('/')
		tplData.template.view = 'main_page';
		tplData.template.layout = 'mainVue';
		tplData.template.component = 'user-page';
		// async
		let toStart = async (() => {
			let obj = {};
			try {
				let phone = req.user.auth.local.login;
				if(!isNaN(Number(phone))){
					let _token = await(pay.auth());
					tplData.list.history = await(pay.getHistoryByPerson(_token,phone));
				}
				// 
			} catch(err){
				console.log('error as/aw', err);
			}
			return true;
		});
		toStart()
			.then((data) => {
				if(req.query && req.query.json == ''){
					return res.send(tplData);
				} else {
					// res.send({'tplData': tplData});
					return render(res, 'index', tplData);
				}
			})
			.catch((err) => {
				if(err){
					console.log(err);
					// res.send({error:err});
					return render(res, 'index', tplData);
				}
			});

	});

module.exports = router;