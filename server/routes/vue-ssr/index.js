'use strict';
let path = require('path');
let fs = require('fs');
global.Vue = require('vue');
// let	renderer = require('vue-server-renderer').createRenderer();

// let app = require('../../../public/js/builds/vue-ssr-bundle.json');
let renderVue;
const { createBundleRenderer } = require('vue-server-renderer')

const serverBundle = require('../../../public/js/builds/vue-ssr-bundle.json')
const clientManifest = require('../../../public/js/builds/vue-ssr-client-manifest.json')
let _layout;
let ssrVue = function(out, data){
	
	return new Promise(function(resolve,reject){
		// renderVue = app(data, dataTmplate);
		if (process.env.NODE_ENV === 'development') {
			_layout = out.replace('<tplData></tplData>', '<script>tplData = '+JSON.stringify(data).replace(/\u2028/g,'\\u2028').replace(/\u2029/g,'\\u2029')+'</script>');
			resolve(_layout);
		} else {
			const renderer = createBundleRenderer(serverBundle, {
				runInNewContext: false,
				out,
				clientManifest
			})
			renderer.renderToString(
				data,
				function (error, html) {
					if (error) {
						reject(error);
					} else {
						_layout = out.replace('<div id="app"></div>', html);
						_layout = _layout.replace('<tplData></tplData>', '<script>tplData = '+JSON.stringify(data).replace(/\u2028/g,'\\u2028').replace(/\u2029/g,'\\u2029')+'</script>');
						resolve(_layout);
					}
				}
			)
		}
	});
};

module.exports = ssrVue;