'use strict';
let auth = require('./auth'),
	main = require('./main'),
	account = require('./account'),
	admin = require('./admin'),
	uploader = require('./uploader'),
	user = require('./user'),
	pull = require('./system/pullReboot');
// api routes
let api = '/api/v1/';
	// apiRoute = require('../js/api-v1/apiRoute');

module.exports = {
	init: (app) => {
		app.use('/', main);
		app.use('/system', pull);
		app.use('/auth', auth);
		app.use('/account', account);
		app.use('/admin', admin);
		app.use('/upload', uploader);
		app.use('/user', user);
		// api
		// app.use( api, apiRoute);
	}
};