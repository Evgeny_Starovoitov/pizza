'use strict';
let $ = require('../../js/api-v1/$')

module.exports = async (date1, date2) => {
	try {
		let allSberOrders = await $._find_All('Order',{"paymentType":"sber", "sberOrderId":{$exists:true},'dates.created': { $gte: date1,	$lt: date2}})
		let fakePayment = false

		if(!allSberOrders || allSberOrders.length === 0) {
			return
		}

		for(let i=0; i<allSberOrders.length; i++){
			const sberOrder = allSberOrders[i]
			let mods = false;
			let sbApiLogin = 'poedimdoma-api';
			let	sbApiPassword = 'Admin123';
			let sberOrderId = sberOrder.sberOrderId;
			let checkUrl = 'https://securepayments.sberbank.ru/payment/rest/getOrderStatus.do?orderId='+sberOrderId+'&language=ru&password='+sbApiPassword+'&userName='+sbApiLogin
			let checkPaymentLink = await($._getRequestSt(checkUrl));
			let data = {}
			// Поле OrderStatus может принимать следующие значения:
			// 1 Заказ зарегистрирован, но не оплачен
			// 2 Предавторизованная сумма захолдирована (для двухстадийных платежей) Проведена полная авторизация суммы заказа
			// 3 Авторизация отменена
			// 4 По транзакции была проведена операция возврата
			// 5 Инициирована авторизация через ACS банка-эмитента
			// 6 Авторизация отклонена
			allSberOrders[i].orderStatus = checkPaymentLink.OrderStatus;
			allSberOrders[i].errorMessage = checkPaymentLink.ErrorMessage;
			const paymentStatus = allSberOrders[i].orderStatus
			// console.log(paymentStatus, allSberOrders[i])
			if(paymentStatus == 3 || paymentStatus == 4 || paymentStatus == 6){
				allSberOrders[i].success = -1;
				allSberOrders[i].description = 
					paymentStatus == 3 ? 'Авторизация отменена' 
						: paymentStatus == 4 ? 'Инициирована авторизация через ACS банка-эмитента' 
							: paymentStatus == 5 ? 'Время авторизации вышло' 
								: paymentStatus == 6 ? 'Авторизация отклонена' : 'Не известный код операции';
				data[allSberOrders[i]._id] = "false payment"

				mods = true;
				fakePayment = true
			}

			if(allSberOrders[i].orderStatus == 2) {
				data[allSberOrders[i]._id] = "true payment"
				allSberOrders[i].success = 1
				allSberOrders[i].order.comment += ' / sberbank: оплачено!'
				allSberOrders[i].description = 'Оплачено'
				const _token = await(pay.auth())
				await pay.sendOrder(allSberOrders[i], _token)

				mods = true
			}

			if (mods) {
				allSberOrders[i].dates.changed = Date.now()
				await $._saveInBase(allSberOrders[i])

				if (allSberOrders[i].userId && fakePayment) {
					// bonusSber: {
					// 	potratil: { type: Number, index: true },
					// 	nakopil: { type: Number, index: true }
					// },

					// bonus: {type: Number, index: false},
					// awaitsBonus: {type: Number, index: false},
					let user =  await $._find_One('Person', { _id: allSberOrders[i].userId } )

					if (user) {
						const tratit = allSberOrders[i].bonusSber && allSberOrders[i].bonusSber.potratil
						const nakopil = allSberOrders[i].bonusSber && allSberOrders[i].bonusSber.nakopil

						// console.log(tratit, nakopil, user, allSberOrders[i])

						if (tratit && tratit > 0) user.bonus = user.bonus + tratit
						if (nakopil && nakopil > 0) user.awaitsBonus = user.awaitsBonus - nakopil

						await $._saveInBase(user)
					}
				}
			}
		}
	} catch (e) {
		console.log('error_in_admin_1', e);
	}
	console.log({ admin_sber_data: data })
}
