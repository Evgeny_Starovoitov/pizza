'use strict';
var async = require('asyncawait/async');
var await = require('asyncawait/await');
let render = require('../render'),
	sberCheck = require('./sberCheck'),
	promoHandler = require('./handler'),
	_tplData = require('../tplData'),
	checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$"),
	express = require('express'),
	request = require('request'),
	redis = require('../../redis'),
	db = require('../../db'),
	router = express.Router(),
	$ = require('../../js/api-v1/$');
	// tools = require('../../js/api-v1/tools');

router.use((req, res, next) => {
	if (!req.user || !req.user.id || (!$.roles(req.user._id) && !$.superRoles(req.user._id))){
		res.status(403).send("Forbidden");
	} else {
		next()
	}
})

router.route('/item')
	.post((req, res, next) => {
		let body = req.body;
		if(!body.name || !body.kind || !body.description || !body.typeTree || !body.parentId) return res.send({error: 'Заполните данные'});
		// async
		let toStart = async (() => {
			let obj = {};
			try {
				let parent = await($._find_ById('Action', body.parentId));
				if (parent && parent._id && parent.url && parent.name){
					var newItem = new db.Action();
					for (var key in body){
						if(Object.prototype.hasOwnProperty.call(body, key)) {
							newItem[key] = body[key];
						}
					}
					if(!newItem.cache) newItem.cache = {};
					if(!newItem.cache.parent) newItem.cache.parent = {};
					if(!newItem.cache.parent.url){
						newItem.cache.parent = {
							"url": parent.url,
							"name": parent.name
						}
					}
					newItem.dates.created = Date.now();
					let _Fx = await ($._saveInBase(newItem));
					if(_Fx){
						obj.success = true;
						obj.obj = JSON.parse(JSON.stringify(newItem));
					}
				}

			} catch(err){
				obj.err = String(err);
			}
			return obj;
		});
		toStart()
			.then((data) => {
				if(data.err){
					throw new Error(data.err)
				} else if(data.success){
					return res.send(data)
				} else {
					return res.send({error: "not complite"})
				}

			})
			.catch((err) => {
				if(err){
					console.log('reject_err', err)
					res.send({error:String(err)});
				}
			});
	})
	.put((req, res, next) => {
		let id = (req.query && req.query.id) ? (req.query.id).toLowerCase() : null
		if(!checkForHexRegExp.test(id)) return res.send({error: 'Заполните данные'});
		let body = req.body;
		// async
		let toStart = async (() => {
			let obj = {};
			try {
				let objToPut = await($._find_ById('Action', id));
				for (var key in body){
					if(Object.prototype.hasOwnProperty.call(body, key)) {
						objToPut[key] = body[key];
					}
				}
				objToPut.dates.changed = Date.now();
				let _Fx = await ($._saveInBase(objToPut));
				if(_Fx){
					obj.success = true;
					obj.obj = JSON.parse(JSON.stringify(objToPut));
				}
			} catch(err){
				obj.err = String(err);
			}
			return obj;
		});
		toStart()
			.then((data) => {
				if(data.err){
					throw new Error(data.err)
				} else if(data.success){
					return res.send(data)
				} else {
					return res.send({error: "not complite"})
				}

			})
			.catch((err) => {
				if(err){
					console.log('reject_err', err)
					res.send({error:String(err)});
				}
			})
	})

	.delete((req, res, next) => {
		let id = (req.query && req.query.id) ? (req.query.id).toLowerCase() : null
		if(!checkForHexRegExp.test(id)) return res.send({error: 'Заполните данные'});
		// async
		let toStart = async (() => {
			let obj = {};
			try {
				let objToDelete = await($._find_ById('Action', id));
				if(objToDelete && objToDelete._id){
					let _delete = await($._delete('Action', {_id: objToDelete._id}));
					if(_delete){
						obj.success = true;
					}
				}
			} catch(err){
				obj.err = String(err);
			}
			return obj;
		})

		toStart()
			.then((data) => {
				if(data.err){
					throw new Error(data.err)
				} else if(data.success){
					return res.send(data)
				} else {
					return res.send({error: "not complite"})
				}

			})
			.catch((err) => {
				if(err){
					console.log('reject_err', err)
					res.send({error:String(err)});
				}
			})
	})

router.route('/promo')
	.put((req, res, next) => {
		let type = (req && req.query && req.query.type) ? req.query.type : null;
		if(!type || !req.body || (req.body && !req.body.props)) return res.send({error: 'Укажите тип события или заполните данные'});
		// async
		let toStart = async (() => {
			let obj = {};
			try {
				switch (type) {
					case 'weekly':
						obj.result = await (promoHandler.update(req.body,"d11111111111110000000001"));
						// console.log('obj.result');
					break;
					case 'slider':
						obj.result = await (promoHandler.update(req.body,"d11111111111110000000002"));
						// console.log('obj.result');
				break;
					default:
						obj.result = null;
						break;
				}
			} catch(err){
				obj.err = String(err);
			}
			let products = await (redis.get('menu:products_not_modified'))
			let toSave = await ($.updateModifers(products))

			await (redis.set('menu:products', toSave))
			return obj
		});
		toStart()
			.then((data) => {
				if(data.err){
					throw new Error(data.err)
				} else if(data.result){
					return res.send({success: true})
				} else {
					return res.send({error: "not complite"})
				}

			})
			.catch((err) => {
				if(err){
					console.log('reject_err', err)
					res.send({error:String(err)});
				}
			});
		});

router.use((req,res,next)=>{
	if(req.user && req.user._id && $.superRoles(req.user._id)){
		next();
	} else {
		let err = new Error('Not Found');
		err.status = 404;
		err.url = req.headers.host+req.url;
		next(err);
	}
});

router.route('/')
	.get(async (req, res, next) => {
		let tplData = _tplData(req);
		tplData.template.view = 'main_page';
		tplData.template.layout = 'admin';
		tplData.template.component = 'admin-page';

		tplData.list = {
			// groups: await redis.get('menu:groups'),
			products: await redis.get('menu:products')
		}
		if(req.query && req.query.json == ''){
			return res.send(tplData);
		} else {
			return res.render('index', tplData);
		}
})

router.route('/orders')
	.get(async (req, res, next) => {
		let tplData = _tplData(req);
		tplData.template.view = 'main_page'
		tplData.template.layout = 'admin'
		tplData.template.component = 'admin-page'

		const query = req.query;

		tplData.list = {
			// groups: await redis.get('menu:groups'),
			products: await redis.get('menu:products')
		}
	
		const date1 = new Date(query.date1 || Date.now());
		const date2 = new Date(query.date2 || Date.now());
		var dateFrom = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate(), 1, 0, 0);
		var dateTo = new Date(date2.getFullYear(), date2.getMonth(), (date2.getDate() + 1), 1, 0, 0);

		tplData.orders = await $._find_All('Order',{'dates.created': { $gte: dateFrom,	$lt: dateTo}})

		if(req.query && req.query.json == ''){
			return res.send(tplData);
		} else {
			return res.render('index', tplData);
		}
})

router.route('/revert/:orderId')
	.get(async (req, res, next) => {
		let tplData = _tplData(req);
		tplData.template.view = 'main_page'
		tplData.template.layout = 'admin'
		tplData.template.component = 'admin-page'

		const orderId = (req.params.orderId + '').toLowerCase()

		if (!orderId) {
			res.status(400).send({ orderId })
		}

		const order = await $._find_ById('Order', orderId)

		if (order && order.paymentType === 'sber') {
			return res.status(400).send({ success: false })
		}

		if (order && order.bonus && order.bonus.nakopil && order.userId) {
			const user = await $._find_ById('Person', order.userId)
			if (user && user.awaitsBonus) {
				user.awaitsBonus = user.awaitsBonus - order.bonus.nakopil
				await $._saveInBase(user)
			}
			order.bonus.nakopil = 0
			order.bonus.potratil = 0
			await $._saveInBase(order)
		}

		return res.status(200).send({ success: true })
})

router.route('/get_payments_sber')
	.get(async (req, res, next) => {
		// async
		var date = new Date();
		var date1 = new Date(date.getFullYear(), date.getMonth(), (date.getDate()), 1, 0, 0);
		var date2 = new Date(date.getFullYear(), date.getMonth(), (date.getDate() + 1), 1, 0, 0);

		// await sberCheck(date1,date2)
		const result = await $._find_All('Order',{"paymentType":"sber", "orderStatus": 2,'dates.created': { $gte: date1,	$lt: date2}})

		return res.send(result)
		}
	)

router.route('/edit_modifiers')
	.put((req, res, next) => {
		let sheet = require('../../sheet');
		let body = req.body;
		// console.log('lol',body,(!body || !sheet), (!body.id || !sheet[body.id]))
		if(!sheet[body.id]){
			sheet[body.id] = {}
		}
		if(!body || !sheet || (body && (!body.id || !sheet[body.id]))) return res.send({error: 'Заполните данные'});
		if(!sheet[body.id].mods) sheet[body.id].mods = {};
		// async
		let toStart = async (() => {
			let obj = {};
			try {
				let modificateMods = false;
				if(sheet && sheet[body.id]){
					for (var key in body.mods){
						if(Object.prototype.hasOwnProperty.call(body.mods, key)) {
							sheet[body.id].mods[key] = body.mods[key];
							modificateMods = true;
						}
					}
				}
				if(modificateMods){
					console.log('xxx')
					obj.mods = sheet[body.id].mods;
				}
				let saveJson = await ($._saveJsonFile(sheet));
				let products = await (redis.get('menu:products_not_modified'));
				let toSave = await ($.updateModifers(products));
				let setProducts = await (redis.set('menu:products', toSave));
			} catch(err){
				obj.err = String(err);
			}
			return obj;
		});
		toStart()
			.then((data) => {
				if(data.err){
					throw new Error(data.err)
				} else if(data.mods){
					return res.send({success: true, mods:data.mods})
				} else {
					return res.send({error: "not complite"})
				}

			})
			.catch((err) => {
				if(err){
					console.log('reject_err', err)
					res.send({error:String(err)});
				}
			});
		});
		
router.route('/reset-dayly-bonuses')
	.get(async(req, res) => {
			try {
				let phone = '79002507499' //req.body.phone
				let user = await($._find_One('Person', {'auth.local.login': phone}))
				if(!user) res.send({error: 'Номер телефона гостя не существует или указан не верно. Бонусы не начислены на этот номер.'})
				console.log('user.awaitsBonus', user.awaitsBonus)
				if (!user.awaitsBonus || user.awaitsBonus == '0'){
					return res.send({error: 'Бонусы данному юзеру не были начислены сегодня'})
				}
				if (!user.cancellation) user.cancellation = []
				user.cancellation.push({bonus: user.awaitsBonus, date: new Date() })
				user.awaitsBonus = 0

				await($._saveInBase(user))
				delete user.auth
				return res.send({success:true, user: user})
			} catch(err){
				return res.send({error: err.message})
			}
		});

module.exports = router;
