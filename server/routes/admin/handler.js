'use strict';
let render = require('../render'),
	redis = require('../../redis'),
	db = require('../../db'),
	cfg = require('../../cfg'),
	$ = require('../../js/api-v1/$');
	// tools = require('../../js/api-v1/tools');

let promoHandler = {};

promoHandler.update = (data, id) =>{
	return new Promise((resolve, reject)=>{
		if(data && data.props){
			db.Action.findById(id, (err, result) => {
				if(err){
					console.log("err in find promo object", err);
					return reject(err);
				} else {
					result.props = data.props;
					result.dates.changed = Date.now();
					result.save((err)=>{
						if(err){
							console.log("err in save promo object", err);
							return reject(err);
						} else {
							return resolve(true);
						}
					})
				}
			})
		} else {
			return reject('not found data to save');
		}
	})
}



module.exports = promoHandler;
