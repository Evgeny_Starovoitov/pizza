'use strict';
let exec = require('child_process').exec;
let cfg = require('../../cfg');
var dirBase = cfg.dirs.base;
let	express = require('express'),
	router = express.Router();

router.get('/:select', function(req, res, next) {
	var select = (req.params.select+'').toLowerCase();
	if((select === 'pull') || (select === 'pull-reboot')){
		var dirBase = cfg.dirs.base;
		exec('/usr/bin/git --work-tree='+dirBase+' --git-dir='+dirBase+'.git pull', function (error, stdout, stderr) {
			if (error) {
				return next(error);
			} else { 
				var stdoutStr = (stdout) ? 'Изменения в файлах : '+stdout+'' : '';
				var stderrStr = (stderr) ? ' Репозиторий : '+stderr+'' : '';
				res.status(200).send(stdoutStr+' '+stderrStr);
				if (select === 'pull-reboot'){
					setTimeout(function() { process.exit(0); }, 1000);
				}
				return;
			}
		}); 
	} else {
		return next();
	}
});

module.exports = router;
