'use strict';
let async = require('asyncawait/async'),
	await = require('asyncawait/await'),
	redis = require('../redis'),
	cfg = require('../cfg'),
	$ = require('../js/api-v1/$'),
	tools = require('../js/api-v1/tools');

let onsockets = function (dataIo, socket, io) {
	return new Promise(function (resolve, reject) {
		let find, list= [];
		let discount = {};
		if(cfg && cfg.discounts){
			discount = cfg.discounts;
		}
		var strUrl = dataIo.str;
		let onStr = 'http://module.sletat.ru/Main.svc/GetTours?login='+cfg.authApiSletat.login+'&password='+cfg.authApiSletat.password+'&';
		let strToRequest = (strUrl) ? (onStr + strUrl) : null;
		let redisKeyReq = strUrl;
		function getState(requestID){
			return new Promise(function(resolve, reject){
				let strIsProcessed = 'http://module.sletat.ru/Main.svc/GetLoadState?requestId='+requestID;
				let urlRekursive = strToRequest;
				urlRekursive.replace('requestId=0','requestId='+requestID);
				urlRekursive.replace('updateResult=0','updateResult=1');
				function rekursState(str, count, breaks, tourOper){
					$._getRequestSt(str)
						.then((data)=>{
							let dataForSocketEmit;
							// Обработка ошибок на полученные путевки
							if (data && data.GetToursResult && data.GetToursResult.IsError){
								reject(data.GetToursResult.IsError);
								return;
							}
							// Обработка ошибок на полученные статусы туроператоров
							if (data && data.GetLoadStateResult && data.GetLoadStateResult.IsError){
								reject(data.GetLoadStateResult.IsError);
								return;
							}
							if (data && data.GetToursResult && data.GetToursResult.Data && data.GetToursResult.Data.aaData){
								if(socket && io){
									console.log(dataIo.chunk, 'Всего ответило:', count, ' ', 'из:', tourOper, ' ', 'Оператор ', count, 'отдал : ',data.GetToursResult.Data.aaData.length,' туров.');
									if(!dataIo.isHotel){
										dataForSocketEmit = tools.sortToursByHotels(data.GetToursResult.Data.aaData, dataIo.rates, discount, dataIo.flag);
									} else {
										dataForSocketEmit = data.GetToursResult.Data.aaData;
									}
									io.io.in(socket.id).emit(dataIo.chunk, { data : dataForSocketEmit, all : tourOper, checked: count, requestId : requestID} );
								}
								if(breaks){
									if(!dataIo.isHotel){
										dataForSocketEmit = tools.sortToursByHotels(data.GetToursResult.Data.aaData, dataIo.rates, discount, dataIo.flag);
									} else {
										dataForSocketEmit = data.GetToursResult.Data.aaData;
									}
									resolve(dataForSocketEmit);
									return;
								}
								setTimeout(function(){rekursState(strIsProcessed, count, false)},1500);
								return;
							}
							// console.log('lol', data.GetLoadStateResult.Data[0].IsProcessed, data.GetLoadStateResult.Data[1].IsProcessed, data.GetLoadStateResult.Data[2].IsProcessed, data.GetLoadStateResult.Data[3].IsProcessed, data.GetLoadStateResult.Data[4].IsProcessed, data.GetLoadStateResult.Data[5].IsProcessed, data.GetLoadStateResult.Data[6].IsProcessed, data.GetLoadStateResult.Data[7].IsProcessed);
							let tmpArr = (data && data.GetLoadStateResult && data.GetLoadStateResult.Data) ? data.GetLoadStateResult.Data : [];
							let i = 0 , j = 0;
							while(i< tmpArr.length){
								if(tmpArr[i].IsProcessed){
									j++;
								}
								i++;
							}
							if(j == tmpArr.length){
								rekursState(urlRekursive, j, true, tmpArr.length);
							} else if(j > count) {
								rekursState(urlRekursive, j, false, tmpArr.length);
							} else {
								setTimeout(function(){rekursState(strIsProcessed, count, false, null)},1500);
							}

						})
				}
				rekursState(strIsProcessed, 0, false, null);
			})
		}
		// console.log('Запрос запущен');
		let toStart = async (()=> {
			let obj = {};
			try {
				// Запрашиваем в редисе наличие данных по ключу
				obj.one = await (redis.get(redisKeyReq));
				if(obj.one && socket && io){
					console.log('Взял с редиса');
					io.io.in(socket.id).emit(dataIo.chunk, obj.one);
				} else {
					// В редисе нет запрашиваем по новой
					// 2 шаг
					console.log('Step two : Новый запрос');
					obj.two = await ($._getRequestSt(strToRequest));
					// console.log('strToRequest',strToRequest);
					// Проверка на возврат ошибки
					if (obj.two && obj.two.GetToursResult && obj.two.GetToursResult.IsError){
						console.log('obj.two.GetToursResult',obj.two.GetToursResult)
						obj.err = obj.two.GetToursResult.ErrorMessage;
					}
					if(obj.two && obj.two.GetToursResult && obj.two.GetToursResult.Data && obj.two.GetToursResult.Data.requestId){
						let requestID = obj.two.GetToursResult.Data.requestId;
						// 3 шаг
						// dataIo.hotels = await ($._find_All('Hotel',{countryId: dataIo.cid}));
						// console.log(dataIo.hotels.length);
						obj.three = await (getState(requestID));
						// console.log('requestID',requestID);
						if(obj.three){
							console.log('list.length',obj.three.length)
							obj.four = await (redis.set(redisKeyReq, obj.three));
							if(obj.four){
								console.log('Redis.set : ',obj.four);
								if(socket && io){
									redis.exp(redisKeyReq, 900);
								} else {
									redis.exp(redisKeyReq, 86400);
								}
							}
						}
					} else {
						obj.err = "Произошла ошибка, повторите позже";
					}
				}
			} catch(err) {
				console.log('ошибка');
				obj.err = String(err);
			}
			return obj;
		});
		toStart()
			.then((obj)=>{
				if(obj && obj.err){
					// обработать success
					if(obj.err) throw new Error(obj.err);
				} else {
					console.log('Выдача завершена');
					resolve(true);
				}
			})
			.catch((err)=>{
				// обработать error
				reject(String(err));
			});
	});
}
module.exports = onsockets;