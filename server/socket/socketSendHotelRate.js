'use strict';
let redis = require('../redis');

function getRates(key){
	return new Promise(function (resolve, reject) {
		redis.get(key)
			.then((r)=>{
				resolve (r);
			})
			.catch((err)=>{
				if(err){
					reject (err);
				}
			});
	});
}
module.exports = getRates;