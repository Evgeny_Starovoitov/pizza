const path = require('path')
const webpack = require('webpack')
// Load the Vue SSR plugin. Don't forget this. :P
const VueSSRPlugin = require('vue-ssr-webpack-plugin')

module.exports = {
	target: 'node',
	entry: './public/vue/index.server.js',
	output: {
		path: path.resolve(__dirname, './public/js/builds'),
		publicPath: '/public/',
		filename: 'build-server.js',
		libraryTarget: 'commonjs2'
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					loaders: {
						js: 'babel-loader?presets[]=es2015'
					}
					// other vue-loader options go here
				}
			},
			// {
			//   test: /\.js$/,
			//   exclude: /(node_modules|bower_components)/,
			//   use: {
			//     loader: 'babel-loader?presets[]=es2015'
			//   }
			// },
			{
				test: /\.js$/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['es2015']
					}
				},
				exclude: /node_modules/
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				loader: 'file-loader',
				options: {
					name: '[name].[ext]?[hash]'
				}
			},

		]
	},
	resolve: {
		alias: {
			'vue$': 'vue/dist/vue.esm.js'
		}
	},
	devServer: {
		historyApiFallback: true,
		noInfo: true
	},
	performance: {
		hints: false
	},
	externals: Object.keys(require('./package.json').dependencies),
	devtool: '#eval-source-map',
	plugins: [
		// Add the SSR plugin here.
		new VueSSRPlugin(),
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: '"production"'
			}
		}),
		new webpack.optimize.UglifyJsPlugin({
			sourceMap: true,
			compress: {
				warnings: false
			}
		}),
		new webpack.LoaderOptionsPlugin({
			minimize: true
		})
	]
}
