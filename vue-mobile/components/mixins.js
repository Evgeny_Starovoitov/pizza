export default {
	data() {
		return {
			loading: false
		}
	},
	methods: {
		checkTotalAmount(evt) {
			// if (this.totalPrice < 500) {
			// 	this.$f7.addNotification({
			// 		hold: 5000,
			// 		title: 'Важно',
			// 		message: 'Минимальная сумма заказа не должна быть менее 500 рублей!',
			// 	});
			// } else {
				this.$f7.mainView.router.load({url: 'cart/', pushState: true});
			// }
		},
		openProduct(item) {
			// this.$store.commit('setProductInPopup', item);
			// this.$nextTick(()=>{
			// 	this.$f7.popup('.popup-about');
			// });
			this.$f7.mainView.router.load({url: 'product/'+item.url, pushState: true});
		},
		setProgress() {
			if (this.loading) {
				let newProgress = this.progress+20;
				this.$store.commit('setProgress', newProgress);
				setTimeout(()=>{
					this.setProgress();
				}, 250)
			} else {
				return;
			}
		},
		loadCategoryPage(link, category) {
			this.$store.commit('setMain', category);
			this.$nextTick(()=>{
				this.$f7.mainView.router.load({url: link, pushState: true});
			})
		},
		openPage(link) {
			this.$f7.mainView.router.load({url: link, pushState: true});
			this.$f7.closePanel();
		},
		goTo(link, isCatPage, category) {
			this.$f7.closePanel();
			if (isCatPage) {
				return this.loadCategoryPage(link, category);
			}
			this.loading = true;
			this.setProgress();
			this.$f7.closePanel();
			this.$http.get(link+'?json').then(response => {
				this.$store.commit('setProgress', 100);
				this.loading = false;
				this.$store.commit('setProgress', 0);
				this.$store.commit('setTData', response.body)
				// history.pushState({ key: Date.now().toFixed(3), url: link }, '', link);
				this.$f7.mainView.router.load({url: link, pushState: true});
			}, response => {
			// error callback
			});
		}
	},
	computed: {
		weekPromo() {
			return this.$store.getters.weekPromo;
		},
		user() {
			return (this.$store && 
					this.$store.getters && 
					this.$store.getters.user) ? this.$store.getters.user : null;
		},
		minOrderAmount() {
			return this.$store.getters.minOrderAmount
		},
		serverBonus() {
			return this.$store.getters.serverBonus
		},
		cartItems() {
			return (this.$store && 
				this.$store.state && 
				this.$store.state.cart && 
				this.$store.state.cart.cartData &&
				this.$store.state.cart.cartData.items) ? this.$store.state.cart.cartData.items : {};
		},
		totalPrice() {
			return (this.$store && 
					this.$store.getters && 
					this.$store.getters.totalPrice) ? this.$store.getters.totalPrice : 0;
		},
		totalAmount() {
			return (this.$store && 
					this.$store.getters && 
					this.$store.getters.totalAmount) ? this.$store.getters.totalAmount : 0;
		},
		progress() {
			return (this.$store && 
					this.$store.state && 
					this.$store.state.progress) ? this.$store.state.progress : 0;
		},
		route() {
			return (this.$f7 && 
					this.$f7.mainView && 
					this.$f7.mainView.route) ? this.$f7.mainView.route : null;
		}
	},
	filters: {
		textCut(val) {
			// return val
			return ((val.replace(/<\/?[^>]+(>|$)/g, "")).substring(0, 250)).replace(/&nbsp;/g,' ').replace(/&amp;/g,'&')+'...';
		},
		date(val) {
			let time = new Date(val.replace(' ', 'T'));
			return ('0'+time.getDate()).substr(-2)+'.'+('0'+(time.getMonth()+1)).substr(-2)+'.'+time.getFullYear();
		}
	}
}