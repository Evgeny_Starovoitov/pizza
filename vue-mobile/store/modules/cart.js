import Vue from 'vue'

const state = {
	cartData: {
		items: {}
	}
}

// getters
const getters = {
	totalAmount: state => {
		let amount = 0;
		for (let key in state.cartData.items) {
			let one = state.cartData.items[key];
			if (one && Object.prototype.hasOwnProperty.call(state.cartData.items, key)) {
				amount+= Number(one.count);
			}
		}
		return amount;
	},
	totalPrice: state => {
		let price = 0;
		for (let key in state.cartData.items) {
			let one = state.cartData.items[key];
			if (one && Object.prototype.hasOwnProperty.call(state.cartData.items, key)) {
				price+= Number(one.price) * Number(one.count);
			}
		}
		return price;
	}
}

// mutations
const mutations = {
	incrementCartItem(state, obj) {
		let item = state.cartData.items[obj.id];
		let newCount = item.count+obj.val;
		if (newCount > 10) {
			item.count = 10;
		} else if (newCount == 0) {
			Vue.delete(state.cartData.items, obj.id);
		} else {
			state.cartData.items[obj.id].count = newCount;
		}
		localStorage.setItem('cart', JSON.stringify(state.cartData));
	},
	initCartData(state) {
		let storageData = localStorage.getItem('cart');
		try {
			storageData = JSON.parse(storageData);
		} catch (e) {
			console.log(e)
			storageData = null;
		}
		if (storageData == null) {
			state.cartData = {items: {}};
		} else {
			state.cartData = storageData;
		}
	},
	clearCart(state) {
		state.cartData = {items: {}};
		localStorage.setItem('cart', JSON.stringify(state.cartData));
	},
	addToCart(state, item) {
		if (!item) return;
		if (state.cartData.items[item.id]) {
			state.cartData.items[item.id].count+= 1;
		} else {
			Vue.set(state.cartData.items, item.id, JSON.parse(JSON.stringify(item)));
		}
		try {
			localStorage.setItem('cart', JSON.stringify(state.cartData));
		} catch (e) {}
	},
	remove(state, id) {
		if (!id) return;
		if (state.cartData.items && state.cartData.items[id]) {
			Vue.$delete(state.cartData.items, id);
			try {
				localStorage.setItem('cart', JSON.stringify(state.cartData));
			} catch (e) {}
		}
	}
}

export default {
	state,
	getters,
	mutations
}