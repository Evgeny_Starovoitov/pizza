import Vue from 'vue'
import Vuex from 'vuex'

import cart from './modules/cart'

const debug = process.env.NODE_ENV !== 'production'

Vue.use(Vuex)
export default new Vuex.Store({
	strict: debug,
	modules: {
		cart
	},
	state: {
		tplData: JSON.parse(JSON.stringify(tplData)),
		catname: '',
		progress: 0,
		productInPopup: null,
		currentOrder: null,
		favourite: {}
	},
	actions: {
		updateStopList({commit, dispatch}){
			Vue.http.get('/stop-list')
			.then((resp)=>{
				commit('UPDATE_STOP_LIST', resp.body)
				setTimeout(()=>{
					dispatch('updateStopList')
				}, 5000)
			})
			.catch(()=>{
				setTimeout(()=>{
					dispatch('updateStopList')
				}, 5000)
			})
		},
		updateUserBonuses({commit}) {
			Vue.http.get('/?json=')
			.then((resp)=>{
				let data = resp.body
				if (data.user) {
					commit('UPDATE_USER', data.user)
				}
			})
		},
		addInFav(context, item) {
			return new Promise((resolve, reject)=>{
				let oldLen = Object.keys(context.state.favourite).length;
				let out = true;
				context.commit('addInFav', item);
				if (Object.keys(context.state.favourite).length > oldLen) {
					out = true;
				} else {
					out = false;
				}
				setTimeout(()=>{
					resolve(out);
				},10);
			});
		}
	},
	mutations: {
		UPDATE_STOP_LIST(state, stopList) {
			Vue.set(state.tplData, 'stopList', stopList)
		},
		UPDATE_USER(state, user) {
			Vue.set(state.tplData, 'user', user)
		},
		addInFav(state, item) {
			if (state.favourite[item.id]) {
				Vue.delete(state.favourite, item.id);
			} else {
				Vue.set(state.favourite, item.id, item);
			}
			Vue.nextTick(()=>{
				localStorage.setItem('favourite', JSON.stringify(state.favourite));
			});
		},
		initFav(state) {
			let storageData = localStorage.getItem('favourite');
			try {
				storageData = JSON.parse(storageData);
			} catch (e) {
				console.log(e)
				storageData = null;
			}
			if (storageData == null) {
				Vue.set(state, 'favourite', {});
			} else {
				Vue.set(state, 'favourite', storageData);
			}
		},
		setCurrentOrder(state, obj) {
			Vue.set(state, 'currentOrder', obj); 
		},
		minusItem(state, obj) {
			if (obj.count === 1) return;
			let id = (obj && obj.id) ? obj.id : '';
			let items = (state && 
						state.tplData && 
						state.tplData.list &&
						state.tplData.list.products) ? state.tplData.list.products : [];
			if (!id) return;
			let i=0;
			while (i < items.length) {
				let one = items[i];
				if (one.id == id) {
					state.tplData.list.products[i].count--;
					break;
				}
				i++;
			}
		},
		plusItem(state, obj) {
			if (obj.count === 10) return;
			let id = (obj && obj.id) ? obj.id : '';
			let items = (state && 
						state.tplData && 
						state.tplData.list &&
						state.tplData.list.products) ? state.tplData.list.products : [];
			if (!id) return;
			let i=0;
			while (i < items.length) {
				let one = items[i];
				if (one.id == id) {
					state.tplData.list.products[i].count++;
					break;
				}
				i++;
			}
		},
		setProductInPopup(state, obj) {
			state.productInPopup = obj;
		},
		setProgress(state, percent) {
			state.progress = percent;
		},
		setTData(state, obj) {
			state.tplData = JSON.parse(JSON.stringify(obj));
		},
		setMain(state, obj) {
			Vue.set(state.tplData, 'main', obj);
		},
		setUser(state, obj) {
			Vue.set(state.tplData, 'user', obj);
		}
	},
	getters: {
		minFreeDeliveryAmount: state => state.tplData.minFreeDeliveryAmount,
		deliveryDish: state => state.tplData.delivery,
		stopList: (state)=>state.tplData.stopList,
		minOrderAmount: (state)=>state.tplData.minOrderAmount,
		serverBonus: (state)=>state.tplData.bonus,
		weekPromo: (state) => {
			return (state.tplData && 
				state.tplData.promo && 
				state.tplData.promo.weekly) ? state.tplData.promo.weekly : null;
		},
		prods: state => {
			return (state.tplData && 
					state.tplData.list && 
					state.tplData.list.products || [])
		},
		itemsByIds: (state, getters) => (ids=[]) => {
			return getters.prods.filter((item)=>ids.includes(item.id))
		},
		item: (state, getters) => (url) => {
			let out = null;
			let items = state.tplData.list.products;
			let i=0;
			while (i < items.length) {
				let one = items[i];
				if (one.url == url) {
					out = one;
					break;
				}
				i++;
			}	
			return out;
		},
		slider: state => {
			let out = [];
			let week = {};
			let weekIdx = 0;
			if (state.tplData.promo && 
				state.tplData.promo.weekly && 
				state.tplData.promo.weekly.props && 
				state.tplData.promo.weekly.props.image && 
				state.tplData.promo.weekly.props.image.mobileBaner && 
				state.tplData.promo.weekly.props.productUrl) {
				week = {link: state.tplData.promo.weekly.props.productUrl, image: state.tplData.promo.weekly.props.image.mobileBaner};
				weekIdx = state.tplData.promo.weekly.props.bannerPosition-1;
			}
			if (state.tplData.promo && 
				state.tplData.promo.slider && 
				state.tplData.promo.slider.props && 
				state.tplData.promo.slider.props.banners) {
				let banners = state.tplData.promo.slider.props.banners;
				for (let i=0; i < banners.length;i++) {
					if (i == weekIdx && week && week.image) {
						out.push(week);
					}
					let one = banners[i];
					if (one.imageMobile) {
						out.push({link: one.url, image: one.imageMobile});
					}
				} 
			}
			return out;
		},
		user: state => {
			return (state.tplData && 
					state.tplData.user) ? state.tplData.user : null;
		},
		items: state => {
			return (state.tplData && 
					state.tplData.list && 
					state.tplData.list.items) ? state.tplData.list.items : []; 
		},
		pageName: state => {
			return (state.tplData && 
					state.tplData.template && 
					state.tplData.template.component) ? state.tplData.template.component : 'main-page'; 
		},
		categories: state => {
			return (state.tplData && 
					state.tplData.list && 
					state.tplData.list.groups) ? state.tplData.list.groups : [];
		},
		allProducts: state => {
			let out = {};
			let products = (state.tplData && 
							state.tplData.list && 
							state.tplData.list.products) ? state.tplData.list.products : [];
			for (let i=0;i<products.length;i++) {
				let one = products[i];
				if (!out[one.parentGroup]) {
					out[one.parentGroup] = [];
				}
				out[one.parentGroup].push(one);
			}
			return out;
		},
		products: (state, getters) => (url) => {
			let out = [];
			let catId = '';
			let i=0;
			while (i < getters.categories.length) {
				let one = getters.categories[i];
				if (one.url == url) {
					catId = one.id;
					break;
				}
				i++;
			}
			if (catId && getters.allProducts && getters.allProducts[catId]) {
				out = getters.allProducts[catId];
			}
			return out;
		}
	}
});