'use strict';
import App from './components/App.vue'
export default context => {
	return Promise.resolve(
		new Vue({
			render: h => h(App)
		})
	);
}