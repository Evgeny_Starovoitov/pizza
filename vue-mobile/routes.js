export default [
	{
		path: '/favourite/',
		component: require('./pages/FavouritePage.vue')
	},
	{
		path: '/account/',
		component: require('./pages/User.vue')
	},
	{
		path: '/recovery/',
		component: require('./pages/RecoveryPassword.vue')
	},
	{
		path: '/review/',
		component: require('./pages/Review.vue')
	},
	{
		path: '/registration/',
		component: require('./pages/Registration.vue')
	},
	{
		path: '/product/:good',
		component: require('./pages/GoodPage.vue')
	},
	{
		path: '/item/:id',
		component: require('./pages/ItemPage.vue')
	},
	{
		path: '/actions/',
		component: require('./pages/News.vue')
	},
	{
		path: '/news/',
		component: require('./pages/News.vue')
	},
	{
		path: '/pages/vac/',
		component: require('./pages/Vacancy.vue')
	},
	{
		path: '/cart/',
		component: require('./pages/Cart.vue')
	},
	{
		path: 'pages/delivery',
		component: require('./pages/Delivery.vue')
	},
	{
		path: '/pages/restaurants',
		component: require('./pages/Restrauns.vue')
	},
	{
		path: '/category/:catname',
		component: require('./pages/CategoryPage.vue')
	}
]