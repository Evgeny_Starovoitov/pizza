'use strict';
import Vue from 'vue'
import App from './components/App.vue'
import Vuex from 'vuex'
import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.use(Vuex)
// Import F7
import Framework7 from 'framework7'
// Import F7 Vue Plugin
import Framework7Vue from 'framework7-vue'

// Init F7 Vue Plugin
Vue.use(Framework7Vue)

import store from './store'
import Routes from './routes'

new Vue({
	store,
	framework7: {
		root: '#app',
		// material: true,
		swipePanel: 'left',
		routes: Routes,
		pushState:true,
		fastClicks: false
	},
	render: h => h(App)
}).$mount('#app');
