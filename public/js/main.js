// $(document).ready(function() {

//     var cart = new Cart();
//     var cart_ui = new CartUiDesktop(window.min_order);
//     var cart_api = new PizzaCartAPI(cart);

//     $(".date").mask("99.99.9999");
//     $(".phone").mask("Телефон  +7 (999) 999-99-99");

//     // dropdown
//     $(".js-dropdown-btn").click(function() {
//         $(".js-dropdown-menu").fadeToggle();
//         $(this).toggleClass("active-dropdown-btn");
//     });

//     // inc/dec items
//     $(".el-order-form-group .input-group-addon").on("click", function() {
//         cart_ui.inc_dec_cart_item($(this));
//     });

//     // add to cart
//     $(".btn-order").on("click", function () {
//         var data = cart_ui.get_product_data($(this));
//         cart.add_item(data);
//         cart_update_ui();
//     });

//     // change option
//     $(".menu-el-value").click(function() {
//         cart_ui.change_option($(this));
//     });

//     // fill in order
//     $('.bt_order').on('click', function(){
//         if(!ordered_enough()){
//             $('#dlg-min-order').modal({show:true});
//             return false;
//         }else{
//             return true;
//         }
//     });

//     $('.btn_last_step').on('click', function(event){
//         if($('#form-customer').validator('validate').has('.has-error').length){
//             return false;
//         }else{
//             make_order();
//             return true;
//         }
//     });

//     // product preview
//     $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
//         event.preventDefault();
//         return $(this).ekkoLightbox();
//     });

//     $('#btn-check-promo').on('click', function(){
//         apply_coupon_code($('#input-promo').val());
//         return false;
//     });

//     //show login dialog
//     if(window.location.hash.match(/#login/)){
//         window.location.hash = '';
//         $('.sign-in-modal').modal('show').on('shown.bs.modal', function(){
//             $('input[name="phone"]').focus();
//         });

//     }

//     // show payemts by cookie
//     if(window.location.hash=='#yandex-test'){
//         $.jStorage.set('yandex-test', 1);
//     }

//     if($.jStorage.get('yandex-test', 0)){
//         $('.show-by-cookie').css('display', 'inline-block');
//     }

//     cart_update_ui();
// });


// function update_discount(){
//     var cart = new Cart();
//     var cart_ui = new CartUiDesktop(window.min_order);
//     var cart_api = new PizzaCartAPI(cart);
//     cart_api.calc_discount(
//         function(data){
//             var cart_meta = cart._get_meta();
//             if(data.has_discount) {
//                 cart_meta['discount_price'] = parseFloat(data.discount_price);
//                 cart_meta['discount_percent'] = parseFloat(data.discount_percent);
//                 cart_meta['discount_description'] = data.description;
//                 cart._set_meta(cart_meta);
//                 cart_ui.set_discount(cart_meta['discount_price'], cart_meta['discount_description']);
//             }else {
//                 cart_ui.set_discount(0, '');
//             }
//             cart_ui.set_total(parseFloat(data.cart_total), parseFloat(data.cart_total_old));
//         },
//         function(){
//             console.log('Error while getting discount');
//         }
//     );
// }


// function make_order(){
//     var cart = new Cart();
//     var cart_ui = new CartUiDesktop(window.min_order);
//     var cart_api = new PizzaCartAPI(cart);
//     var details = cart_ui.get_order_details(cart);
//     cart_api.make_order(details,
//         function(data){
//             console.log('data',data);
//             cart.clear();
//             cart_update_ui();
//             if(data.form){
//                 // show redirection dialog
//                 message_dialog('Оплата заказа', 'Идет перенаправление на платежный сайт...');
//                 $('#yandex-form>table').html(data.form);
//                 if (window.search.indexOf('test') !== -1) {
//                     $('#yandex-form>table').html(data.form);
//                     return;
//                 }
//                 $('#yandex-form').submit();
//             }else{
//                 // show success dialog
//                 $('.order4-modal').modal('show');
//             }
//         },
//         function(){
//             // show error dialog
//             message_dialog(
//                 'Ошибка при заказе',
//                 'Произошла ошибка при заказе. Сделайте, пожалуйста, ваш заказ по телефону.');
//         });
// }


// function ordered_enough(){
//     var cart = new Cart();
//     return cart.get_stats()['total_old'] >= window.min_order;
// }


// function apply_coupon_code(coupon_code){
//     var cart = new Cart();
//     var cart_ui = new CartUiDesktop(window.min_order);
//     var cart_api = new PizzaCartAPI(cart);

//     cart_api.apply_coupon(coupon_code,
//         function(data){
//             cart_update_ui();
//         },
//         function(error){
//             cart_ui.set_promo_error(error);
//         }
//     );
// }


// function cart_update_ui(){
//     var cart = new Cart();
//     var cart_ui = new CartUiDesktop(window.min_order);
//     var cart_api = new PizzaCartAPI(cart);
//     cart_ui.update_ui(cart);
//     cart_ui.update_product_counters(cart);
//     cart_ui.update_cart_list(cart, cart_update_ui);
//     update_discount();
// }


// //Check to see if placeholder is supported
// //Hat tip: http://diveintohtml5.info/detect.html#input-placeholder
// if (!Modernizr.input.placeholder) {

//     function supports_input_placeholder() {
//         var i = document.createElement('input');
//         return 'placeholder' in i;
//     }

//     if (supports_input_placeholder()) {
//         // The placeholder attribute is supported
//         //Do nothing. Smile. Go outside
//     } else {
//         // The "placeholder" attribute isn't supported. Let's fake it.

//         var $placeholder = $(":input[placeholder]"); // Get all the input elements with the "placeholder" attribute

//         // Go through each element and assign the "value" attribute the value of the "placeholder"
//         // This will allow users with no support to see the default text
//         $placeholder.each(function() {
//             var $message = $(this).attr("placeholder");
//             $(this).attr("value", $message);
//         });

//         // When a user clicks the input (on focus) the default text will be removed
//         $placeholder.focus(function() {
//             var $value = $(this).attr("value");
//             var $placeholderTxt = $(this).attr("placeholder");

//             if ($value == $placeholderTxt) {
//                 $(this).attr("value", "");
//             }
//         });

//         // When a user clicks/tabs away from the input (on blur) keep what they typed
//         // Unless they didn't type anything, in that case put back in the default text
//         $placeholder.blur(function() {
//             var $value = $(this).attr("value");
//             var $placeholderTxt = $(this).attr("placeholder");

//             if ($value == '') {
//                 $(this).attr("value", $placeholderTxt);
//             }
//         });

//         // Since we're inputing text into the "value" attribute we need to make 
//         // sure that this default text isn't submitted with the form, potentially
//         // causing validation issues. So we're going to remove the default text
//         // and submit the inputs as blank.
//         $("form").submit(function() {
//             var $checkValue = $(":input");

//             $checkValue.each(function() {
//                 if ($(this).attr("value") == $(this).attr("placeholder")) {
//                     $(this).attr("value", "");
//                 }
//             });
//         });
//     }
// }

