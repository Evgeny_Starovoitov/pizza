$(document).ready(function(){
   if(window.location.hash=='#payment_success'){
        message_dialog('Оплата заказа', 'Оплата завершена успешно. ' +
        'Ваш заказ поступил обработку. Номер заказа вы получите на email или телефон, указанный в заказе.')
   }

   if(window.location.hash=='#payment_fail'){
        message_dialog('Оплата заказа', 'Произошла ошибка при оплате заказа.')
   }
});

function message_dialog(title, body){
    $('#dialog_message_title').html(title);
    $('#dialog_message_body').html(body);
    $('#dialog_message').modal('show');
}