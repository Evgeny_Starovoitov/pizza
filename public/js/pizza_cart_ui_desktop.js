// function CartUiDesktop(min_order){
//     this.min_order = min_order;
// }

// CartUiDesktop.prototype.formatMoney = function(sum){
//     return parseFloat(parseFloat(sum).toFixed(2))
// }


// CartUiDesktop.prototype.set_count = function(items_count){
//     $('.cart_qty').text(items_count);
//     this.order_panel_enable(items_count > 0);
// };

// CartUiDesktop.prototype.set_total = function(total, total_old){
//     $('.cart_total').text(this.formatMoney(total));
//     this.button_order_enable(total_old >= this.min_order);
// };

// CartUiDesktop.prototype.order_panel_enable = function(enable){
//     if(enable) {
//         $('.order-panel-wrapper').show();
//     }
//     else {
//         var $panel = $('.order-panel-wrapper').hide();
//         var $dropdown_btn = $panel.find(".js-dropdown-btn");
//         if($dropdown_btn.hasClass('active-dropdown-btn'))
//             $dropdown_btn.click();
//     }
// };

// CartUiDesktop.prototype.button_order_enable = function(enable){
//     var $bt_order = $('.bt_order');
//     if(!enable){
//         $bt_order.removeClass('btn-danger');
//         if(!$bt_order.hasClass('btn-primary'))
//             $bt_order.addClass('btn-primary');
//     }else{
//         $bt_order.removeClass('btn-primary');
//         if(!$bt_order.hasClass('btn-danger'))
//             $bt_order.addClass('btn-danger');
//     }
// };

// CartUiDesktop.prototype.set_discount = function(discount, description){
//     if(discount > 0){
//         $('.cart_discount').text(this.formatMoney(discount));
//         $('.order-panel-discount-price').show().attr('data-original-title',
//             "Применена: " + description +
//             ". Скидки не суммируются, выбирается одна максимальная скидка.");
//     }else{
//         $('.cart_discount').text('0');
//         $('.order-panel-discount-price').hide().attr('data-original-title', '');
//     }
// };

// CartUiDesktop.prototype.set_coupon = function(coupon_code, discount){
//     if(coupon_code){
//         $('#applied-promo').html('Купон '+coupon_code+' на скидку '+discount +'%');
//         $('#input-promo').hide();
//         $('#btn-check-promo').hide();
//         $('#promo-tooltip').hide();
//         $('#promo-error').hide();
//     }
// };

// CartUiDesktop.prototype.update_ui = function(cart){
//     var stat = cart.get_stats();
//     var cart_meta = cart._get_meta();
//     var cart_items = cart._get_cart();
//     this.set_count(stat['count']);
//     this.set_total(stat['total'], stat['total_old']);
//     this.set_discount(stat['discount_price'], stat['discount_description']);
//     this.set_coupon(cart_meta['coupon_code'], cart_meta['promo_discount']);
// };

// CartUiDesktop.prototype.get_product_data = function($bt_order){
//     var $product = $bt_order.closest(".menu-el");
//     var $option = $product.find(".menu-el-count .menu-el-value.active");

//     var data={
//         id: $product.attr('data-product'),
//         qty: parseInt($product.find("input.form-control").val()),
//         type: parseInt($product.attr('data-prodtype')),
//         title: $product.find('.menu-el-title').text(),
//         img: $product.find('.thumbnail img').attr('src')
//     };

//     if($option.length){
//         data['price'] = parseFloat($option.attr('data-price'));
//         data['old_price'] = parseFloat($option.attr('data-oldprice'));
//         if(data['type']==1){
//             data['size'] = parseInt($option.attr('data-size'));
//             data['title'] += ' ('+data['size']+' см.)';
//         }else if(data['type']==2){
//             data['rolls'] = parseInt($option.attr('data-numrolls'));
//             data['title'] += ' ('+data['rolls']+' рол.)';
//         }
//     }else{
//         data['price'] = parseFloat($product.attr('data-price'));
//         data['old_price'] = parseFloat($product.attr('data-oldprice'));
//     }
//     return data;
// };

// CartUiDesktop.prototype.get_order_details = function(cart){
//     var $modal = $('.order_details');
//     var is_delivery = $modal.find('.delivery_type > li.active a').attr('href').indexOf('delivery') > -1;
//     var details = {
//         name: $modal.find('input[name=name]').val(),
//         phone: $modal.find('input[name=phone]').val(),
//         email: $modal.find('input[name=email]').val(),
//         delivery: is_delivery,
//         num_persons: $modal.find('select[name=num_persons]').val(),
//         pay_method: $modal.find('input[name=pay_options]:checked').val(),
//         comment: $modal.find('textarea[name=comment]').val()
//     };

//     if(is_delivery){
//         details['address'] = $modal.find('textarea[name=address]').val();
//         if($modal.find('input[name=delivery_time]:checked').val()=='at_time')
//             details['time'] = $modal.find('select[name=time]').val();
//         else
//             details['time'] = 'Как можно скорее';
//     }else{
//         details['rest_address'] = $modal.find('select[name=rest_address]').val();
//     }

//     var cart_meta = cart._get_meta();
//     if(cart_meta['coupon_code']){
//         details['coupon_code'] = cart_meta['coupon_code'];
//     }

//     return details;
// };

// CartUiDesktop.prototype.set_promo_error = function(error){
//     $('#promo-error').text(error);
// };

// CartUiDesktop.prototype.inc_dec_cart_item = function($button){
//     var oldValue = $button.closest('.el-order-form-group').find("input.form-control").val();

//     if ($button.text() == "+") {
//         var newVal = parseFloat(oldValue) + 1;
//     } else {
//         // Don't allow decrementing below zero
//         if (oldValue > 1) {
//             var newVal = parseFloat(oldValue) - 1;
//         } else {
//             newVal = 1;
//         }
//     }
//     $button.closest('.el-order-form-group').find("input.form-control").val(newVal);
// };

// CartUiDesktop.prototype.set_bascket_counter = function($product, qty){
//     if(qty){
//         $product.find('.basket-counter').show().find('.basket-counter-qty').html(qty);
//     }else{
//         $product.find('.basket-counter').hide().find('.basket-counter-qty').html(qty);
//     }
// };

// CartUiDesktop.prototype.update_product_counters = function(cart){
//     var self = this;
//     var id_qty={};
//     var cart_items = cart._get_cart();
//     for(var key in cart_items) {
//         if (cart_items.hasOwnProperty(key)) {
//             var id = parseInt(cart_items[key]['id']);
//             if(id in id_qty)
//                 id_qty[ id ] += cart_items[key]['qty'];
//             else
//                 id_qty[ id ] = cart_items[key]['qty'];
//         }
//     }

//     $('.menu-el').each(function(){
//         var $this = $(this);
//         var id = parseInt($this.attr('data-product'));
//         if(id in id_qty){
//             self.set_bascket_counter($this, id_qty[id])
//         }else{
//             self.set_bascket_counter($this, 0);
//         }
//     });
// };

// CartUiDesktop.prototype.update_cart_list = function (cart, callback){
//     var $template = $('.orders-list-template li');
//     var $list = $('.all-orders-list').empty();
//     var self = this;
//     var cart_items = cart._get_cart();
//     for(var key in cart_items) {
//         if (cart_items.hasOwnProperty(key)) {
//             $list.each(function(){
//                 var $itm = $template.clone();
//                 self.update_cart_list_item(cart, $itm, cart_items[key], callback);
//                 $(this).append($itm);
//             });
//         }
//     }
// };

// CartUiDesktop.prototype.update_cart_list_item = function(cart, $list_item, cart_item, callback){
//     var self = this;

//     $list_item.attr('data-product', cart_item['id']);
//     $list_item.attr('data-prodtype', cart_item['type']);
//     $list_item.attr('data-cartid', cart.product_key(cart_item));
//     $list_item.find('.thumbnail img').attr('src', cart_item['img']);
//     $list_item.find('.title-in-order-list').text(cart_item['title']);
//     $list_item.find('.input_qty').val(cart_item['qty']);
//     $list_item.find('.item_sum').text(this.formatMoney(cart_item['qty'] * cart_item['price']));

//     // remove item
//     $list_item.find('.close').on('click', function(){
//         var $button = $(this);
//         var cart_item_id = $button.closest('.one-order-in-orders').attr('data-cartid');
//         cart.remove_item(cart_item_id);
//         if(callback){
//             callback();
//         }
//     });

//     // change quantity
//     $list_item.find(".input-group-addon").on("click", function() {
//         var $button = $(this);
//         self.inc_dec_cart_item($button);
//         var new_count = parseInt($button.closest('.el-order-form-group').find("input.form-control").val());
//         var cart_item_id = $button.closest('.one-order-in-orders').attr('data-cartid');
//         if(new_count==0)
//             cart.remove_item(cart_item_id);
//         else
//             cart.change_item_qty(cart_item_id, new_count);
//         if(callback){
//             callback();
//         }
//     });
// };

// CartUiDesktop.prototype.change_option = function($option_element) {
//     var $item = $option_element.closest('.menu-el');
//     $option_element.parent(".menu-el-count").find(".active").removeClass("active");
//     var $option = $option_element.addClass("active");
//     $item.find('.weight').text($option.attr('data-weight'));
//     $item.find('.item_price').text(this.formatMoney($option.attr('data-price')));
//     $item.find('.item_old_price').text(this.formatMoney($option.attr('data-oldprice')));
// };