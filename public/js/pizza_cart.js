function Cart(){
    this.key_prefix = '';
}

Cart.prototype.add_item = function(product_data){
    var cart = this._get_cart();
    var cart_item_id = this.product_key(product_data);
    if(cart.hasOwnProperty(cart_item_id)){
        cart[cart_item_id]['qty'] += product_data['qty'];
    }else{
        cart[cart_item_id]=product_data;
    }
    this._set_cart(cart);
    return cart_item_id;
};

Cart.prototype.get_item = function(cart_item_id){
    var cart = this._get_cart();
    if(cart_item_id in cart){
        return cart[cart_item_id];
    }
    return null;
}

Cart.prototype.remove_item = function(cart_item_id){
    var cart = this._get_cart();
    if(cart_item_id in cart){
        delete cart[cart_item_id];
    }
    this._set_cart(cart);
};

Cart.prototype.change_item_qty = function(cart_item_id, new_qty){
    var cart = this._get_cart();
    if (cart_item_id in cart){
        cart[cart_item_id]['qty'] = parseInt(new_qty);
    }
    this._set_cart(cart);
};

Cart.prototype.product_key = function(cart_item){
    if(cart_item['type']==1){
        return cart_item['id']+'_'+cart_item['size'];
    }else if(cart_item['type']==2){
        return cart_item['id']+'_'+cart_item['rolls'];
    }else{
        return cart_item['id'];
    }
}

Cart.prototype.clear = function(){
    this._set_meta({});
    this._set_cart({});
};

Cart.prototype.get_stats = function(){
    var stat = {
        count:0,
        unique:0,
        total:0,
        total_old:0,
        discount:0
    };
    var cart = this._get_cart();
    var cart_meta = this._get_meta();
    for(var key in cart){
        if(cart.hasOwnProperty(key)){
            stat['count'] += parseInt(cart[key].qty);
            stat['unique'] += 1;
            stat['total'] += cart[key].qty * cart[key].price;
            stat['total_old'] += cart[key].qty * cart[key].old_price;
        }
    }
    if('discount_price' in cart_meta && cart_meta['discount_price'] > 0 ){
        stat['discount'] = cart_meta['discount_price'];
        stat['total'] = stat['total_old'] - cart_meta['discount_price'];
    }
    return stat;
};

Cart.prototype.update_meta = function(dict){
    var cart_meta = this._get_meta();
    $.extend(cart_meta, dict);
    this._set_meta(cart_meta);
}

Cart.prototype._get_cart = function(){
    return this._storage_get('cart', {});
};

Cart.prototype._set_cart = function(cart_dict){
    this._storage_set('cart', cart_dict);
};

Cart.prototype._get_meta = function(){
    return this._storage_get('cart_meta', {});
};

Cart.prototype._set_meta = function(meta_dict){
    this._storage_set('cart_meta', meta_dict);
};

Cart.prototype._storage_get = function(key, default_value){
    var k = this.key_prefix + key;
    return $.jStorage.get(k, default_value);
};

Cart.prototype._storage_set = function(key, value){
    var k = this.key_prefix + key;
    $.jStorage.set(k, value);
};
