function PizzaCartAPI(cart){
    this.cart = cart;
}

PizzaCartAPI.prototype.make_order = function(order_details, success_callback, fail_callback){
    var order = {
        details: order_details,
        items: this.cart._get_cart()
    };
    $.post('/api/order', JSON.stringify(order), null, 'json')
        .done(function(data){
            if(success_callback){
                success_callback(data);
            }
        })
        .fail(function(){
            if(fail_callback){
                fail_callback();
            }
        })
        .always(function(){
        });
};

PizzaCartAPI.prototype.calc_discount = function(success_callback, fail_callback){
    var cart_dict = {
        items: this.cart._get_cart(),
        cart_meta: this.cart._get_meta()
    };
    $.post('/api/discount', JSON.stringify(cart_dict), null, 'json')
        .done(function(data){
            if(success_callback){
                success_callback(data);
            }
        })
        .fail(function(e){
            if(fail_callback){
                fail_callback(e);
            }
        })
        .always(function(){
        });
};

PizzaCartAPI.prototype.apply_coupon = function(coupon_code, success_callback, fail_callback){
    var self = this;
    $.getJSON('/coupon/'+encodeURIComponent(coupon_code), function(data){
        if(data.success){
            var cart_meta = self.cart._get_meta();
            cart_meta['coupon_code'] = coupon_code;
            cart_meta['promo_discount'] = parseInt(data.discount);
            self.cart._set_meta(cart_meta);
            if(success_callback){
                success_callback(data);
            }
        }else{
            if(fail_callback){
                fail_callback(data.error);
            }
        }
    });
};