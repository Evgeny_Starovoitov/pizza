
module('Cart');

function create_test_cart(){
    var cart = new Cart();
    cart.key_prefix = 'test_';
    return cart;
};

function create_cart_items(cart){
    cart._set_cart({});
    cart.add_item({id:2, type:1, size:28, qty:1, price:150, old_price:180});
    cart.add_item({id:3, type:1, size:28, qty:1, price:100, old_price:120});
    cart.add_item({id:3, type:1, size:28, qty:1, price:100, old_price:120});
    cart.add_item({id:4, type:2, rolls:6, qty:2, price:50, old_price:60});
}

QUnit.test( "product_key()", function( assert ) {
    var cart  = create_test_cart();
    var cart_item = {type:1, id:1, size:28};
    assert.equal(cart.product_key(cart_item), '1_28', 'Numeric arguments');
});

QUnit.test( "add_item()", function( assert ) {
    var cart  = create_test_cart();
    var item_id = cart.add_item({id:2, type:1, size:28});
    assert.equal(item_id, '2_28');
});

QUnit.test( "remove_item()", function( assert ) {
    var cart  = create_test_cart();

    create_cart_items(cart);
    var item_id = '2_28';
    assert.ok(cart.get_item(item_id)!=null);

    cart.remove_item(item_id)
    assert.ok(cart.get_item(item_id)==null);
});

QUnit.test( "get_stats()", function( assert ) {
    var cart  = create_test_cart();
    cart._set_cart({});

    var stat = cart.get_stats();
    assert.equal(stat['count'], 0);

    create_cart_items(cart);
    var stat = cart.get_stats();
    assert.equal(stat['count'], 5);
    assert.equal(stat['unique'], 3);
    assert.equal(stat['total'], 450);
    assert.equal(stat['total_old'], 540);
});

QUnit.test( "clear()", function( assert ) {
    var cart  = create_test_cart();

    create_cart_items(cart);
    assert.notEqual(Object.keys(cart._get_cart()).length, 0);
    cart.clear();
    assert.equal(Object.keys(cart._get_cart()).length, 0);

});

QUnit.test( "update_meta()", function( assert ) {
    var cart  = create_test_cart();
    cart._set_meta({'old_key':5});

    cart.update_meta({'a':1, 'b':2});
    var meta = cart._get_meta();
    assert.ok('a' in meta);
    assert.ok('b' in meta);
    assert.ok('old_key' in meta);
});

module('API');

QUnit.test( "calc_discount()", function( assert ) {
    var cart  = create_test_cart();
    create_cart_items(cart);
    var stat = cart.get_stats();
    var cart_api  = new PizzaCartAPI(cart);
    var done = assert.async();
    cart_api.calc_discount(
        function(data){
            assert.ok(data.has_discount, 'Has discount');
            assert.ok(data.discount_price > 0,'Discount = '+data.discount_price)
            assert.ok(data.discount_percent > 0,'Discount percent = '+data.discount_percent)
            assert.ok(data.cart_total > 0,'Total = '+data.cart_total)
            done();
        },
        function(e){
            assert.ok(false, e.responseText);
            done();
        });
});
