'use strict';
import Vue from 'vue'
import Layout from './admin/Layout.vue'
import adminRouter from './adminRouter'
const router = adminRouter()

new Vue({
	router,
	data: {
		tplData: JSON.parse(JSON.stringify(tplData))
	},
	render: h => h(Layout, {
		props: {
			tplData: JSON.parse(JSON.stringify(tplData))
		}
	})
}).$mount('#app');
