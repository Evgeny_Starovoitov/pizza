import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export function createStore(tData) {
	return new Vuex.Store({
		'state': {
			tplData: tData,
			bonusInPay: false
		},
		actions: {

		},
		mutations: {
			'SET_BONUS_IN_PAY'(state, val) {
				state.bonusInPay = val;
			}
		},
		getters: {
			minFreeDeliveryAmount(state) {
				return (state.tplData && state.tplData.minOrderAmount) ? state.tplData.minFreeDeliveryAmount : 500
			},
			minOrderAmount(state) {
				return (state.tplData && state.tplData.minOrderAmount) ? state.tplData.minOrderAmount : 500
			},
			user(state) {
				return (state.tplData && state.tplData.user) ? state.tplData.user : null
			},
			userBonuses(state) {
				return (state.tplData && state.tplData.user && state.tplData.user.bonus) ? state.tplData.user.bonus : 0;
			}
		}
	})
}