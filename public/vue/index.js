'use strict';
import Vue from 'vue'
import Layout from './modules/Layout.vue'
import Notifications from 'vue-notification'
import {createStore} from './store/index.js'

let store = createStore( JSON.parse(JSON.stringify(tplData)))
Vue.use(Notifications)

new Vue({
	store:store,
	data: {
		tplData: JSON.parse(JSON.stringify(tplData))
	},
	render: h => h(Layout, {
		props: {
			tplData: JSON.parse(JSON.stringify(tplData))
		}
	}),
	mounted() {
		// this.updateStopList()
	},
	methods: {
		
	}
}).$mount('#app');
