'use strict';
import Layout from './modules/Layout.vue'
import {createStore} from './store/index.js'


export default context => {
	let store = createStore(context)
	return Promise.resolve(
		new Vue({
			store,
			data: {
				tplData: context
			},
			render: h => h(Layout, {
				props: {
					tplData: context
				}
			})
		})
	);
}