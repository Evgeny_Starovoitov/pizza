import Vue from 'vue'
import VueRouter from 'vue-router'

import Orders from './admin/pages/Orders.vue'
import Main from './admin/pages/Main.vue'
import PrintSber from './admin/pages/PrintSber.vue'

const routes = [
	{ path: '/', component: Main },
  { path: '/orders', component: Orders },
  { path: '/print', component: PrintSber }
]


Vue.use(VueRouter)

export default function(){
    return new VueRouter({
		routes
	})
}
