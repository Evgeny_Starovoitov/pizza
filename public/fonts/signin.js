var Signin = {
	template: `<div>
			<h2>Выберите способ регистрации</h2>
			<br>
			<div class="row">
				<div class="col-md-4"><router-link to="/signin/phone">По смс<span class="glyphicon glyphicon-search"></span></router-link></div>
				<div class="col-md-4"><router-link to="/signin/email">По Email</router-link></div>
				<div class="col-md-4"><router-link to="/signin/social">Через соц сети</router-link></div>
			</div>
			<br><br>
			<router-view></router-view>
			</div>`,
	data: function() {
		return {
			pass1: '',
			pass2: ''
		}
	},
	computed: {
		pass: function() {
			var resp = false;
			if (this.pass1 === this.pass2) {
				resp = true;
			}
			return resp;
		}
	},
	methods: {
		getDataFromForm: function(form){
			var data = {};
			if (!form) return data;
			for (var i=0; i <form.elements.length;i++) {
				var one = form.elements[i];
				if (one.name) {
					var _name = one.name;
					var _value = one.value;
					if (one.type === 'checkbox') {
						_value = (one.checked);
					}
					data[_name] = _value;
				}
			}
			return data;
		},
		signin: function (e) {
			var obj = this.getDataFromForm(e.target);
			obj.step = 1;
			
			if (!this.pass) {
				toastr.warning('Введенные пароли не совпадают!')
				return;
			}
			Vue.http.post('/auth/login', obj)
				.then(function(data) {
					var resp = data.json();
					console.log(resp);
				});
		}
	}
}
module.exports = Signin;