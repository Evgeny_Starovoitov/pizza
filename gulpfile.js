var gulp = require('gulp'),
exec = require('gulp-exec')

gulp.task('vue', function (cb) {
    var options = {
        continueOnError: false, // default = false, true means don't emit error event 
        pipeStdout: false, // default = false, true means stdout is written to file.contents 
        customTemplatingThing: "test" // content passed to gutil.template() 
    };
    var reportOptions = {
        err: true, // default = true, false means don't write err 
        stderr: false, // default = true, false means don't write stderr 
        stdout: false // default = true, false means don't write stdout 
    }
    exec('npm run build:client', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
  });
});
gulp.task('w-vue', function () {
    gulp.watch('public/vue/**/**/**/**', ['vue']);
});
gulp.task('default', ['w-vue']);
